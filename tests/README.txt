
AUTHOR
------
Jimmy Berry ("boombatower", http://drupal.org/user/214218)

SIMPLETEST
----------
The tests require the SimpleTest 2.x module to run. SimpleTest is available at
http://drupal.org/project/simpletest.

Once the module is enabled the test will display on admin/build/simpletest.
