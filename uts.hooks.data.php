<?php
/**
 * @file
 * Provide data collection hook documentation for Usability Testing Suite.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Define data collection the module provides. This provides the name and
 * description that will be displayed to both the user and is used to
 * determine what module can be used to collect data.
 *
 * @return array Data collection plug-in information.
 *
 *   The array should use the module name as a key and contain the following
 *   keys-value pairs (all required):
 *
 *   - "name": Name used to reference the data collection plug-in.
 *   - "description": Short description of the module that can be easily
 *     understood by both the test creator and the participant.
 *   - "software_require": Client side software requirements.
 *   - "javascript": Boolean javascript required.
 */
function hook_uts_data_collection() {
  return array(
    'uts_plugin' => array(
      'name' => t('Plug-in name'),
      'description' => t('Plug-in description.'),
      'software_require' => FALSE,
      'javascript' => FALSE
    )
  );
}

/**
 * Define the client requirements and their status. This behaves similarly to
 * hook_requirements(). If the requirements are not met and the data collection
 * plug-in is not required then it will not be enabled for collection. If the
 * plug-in is required and the requirements are not met then it will not allow
 * the participant to continue until the requirements are satisfied.
 *
 * @return array Data collection plug-in information.
 *
 *   The array should use the module name as a key and contain the following
 *   keys-value pairs (all required):
 *
 *   - "status": The requirements status, one of:
 *       - 'ok': Requirement satisfied.
 *       - 'warning': Requirement satisfied, but there is a warning.
 *       - 'error': Requirement not satisfied.
 *   - "name": Name used to reference the data collection plug-in.
 *   - "description": Short description of the module that can be easily
 *     understood by both the test creator and the participant.
 */
function hook_uts_client_requirements() {
  return array(
    'uts_plugin' => array(
      'status' => 'ok',
      'name' => t('Client requirement'),
      'description' => t('All requirements met, data collection enabled.')
    )
  );
}

/**
 * Retrieve the data collected by the plug-in for the specified parameters.
 * Just the first parameter or all the parameters must be specified.
 *
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @param interger $start_timestamp Start timestamp.
 * @param integer $stop_timestamp Stop timestamp.
 * @return array Data within the specified paramters.
 *
 *   The array should use the module name as a key and contain rows of data
 *   with each element being an array key. There is not stringent requirement
 *   since data can be in many different formats. The data will be requested by
 *   analysis plug-ins that should know what to do with the returned data.
 */
function hook_uts_data_get($study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($session_id && $start_timestamp && $stop_timestamp) {
    $result = db_query("SELECT p.session_id, p.timestamp, p.element1, p.element2
                        FROM {uts_plugin} p, {uts_session} s
                        WHERE p.session_id = s.session_id
                        AND s.study_nid = %d
                        AND p.timestamp >= %d
                        AND p.timestamp <= %d
                        AND p.session_id = '%s'
                        ORDER BY timestamp ASC", $study_nid, $start_timestamp, $stop_timestamp, $session_id);
  }
  else {
    $result = db_query("SELECT p.session_id, p.timestamp, p.element1, p.element2
                        FROM {uts_plugin} p, {uts_session} s
                        WHERE p.session_id = s.session_id
                        AND s.study_nid = %d
                        ORDER BY timestamp ASC", $study_nid);
  }
  $data = array();
  while ($record = db_fetch_array($result)) {
    $data[] = $record;
  }
  return array('uts_plugin' => $data);
}

/**
 * Delete data stored by data collection plug-in within the specified
 * parameters. The plug-ins are responsible for removing all data of any kind
 * be it files or database records.
 *
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 */
function hook_uts_data_delete($study_nid, $session_id = NULL) {
  if ($session_id) {
    db_query("DELETE FROM {uts_plugin}
              WHERE session_id = '%s'", $session_id);
  }
  else {
    $sessions = uts_session_load_all($study_nid);
    if (count($sessions) > 0) {
      $placeholders = implode("','", array_fill(0, count($sessions), '%s'));
      db_query("DELETE FROM {uts_plugin}
                WHERE session_id IN ('$placeholders')", $sessions);
    }
  }
}

/**
 * Perform startup tasks upon the beginning of a study task. This hook is
 * called after the participant presses the "Start" button and begins the
 * a new task.
 *
 * This is not intended for code that takes a long time to execute as it will
 * hold up the participant.
 *
 * @param int $timestamp Standardized timestamp of the task completion time.
 */
function hook_uts_data_started_task($timestamp) {

}

/**
 * Perform cleanup or closure tasks upon the completion of a study task. This
 * hook is called after the participant presses the "Done" button or runs out of
 * time and is automatically forwarded to the next task.
 *
 * This is not intended for code that takes a long time to execute as it will
 * hold up the participant.
 *
 * @param int $timestamp Standardized timestamp of the task completion time.
 */
function hook_uts_data_completed_task($timestamp) {

}

/**
 * Perform cleanup or closure tasks upon the completion of a study. This hook
 * is called when the participant has completed all the tasks in the study.
 *
 * @param int $timestamp Standardized timestamp of the study completion time.
 */
function hook_uts_data_completed_study($timestamp) {

}
