<?php
/**
 * @file
 * Provide nodeapi implementation.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_node_info().
 */
function uts_node_info() {
  return array(
    UTS_STUDY => array(
      'name' => t('UTS Study'),
      'module' => 'uts',
      'description' => t('A study is the basic unit of the Usability Testing Suite.' .
                         'Participants complete a study and the results are evaluated.'),
      'body_label' => t('Description'),
      'custom' => TRUE,
      'locked' => TRUE
    ),
    UTS_TASK => array(
      'name' => t('UTS Task'),
      'module' => 'uts',
      'description' => t('An item to be complete by a participant as part of a study.'),
      'body_label' => t('Description'),
      'custom' => TRUE,
      'locked' => TRUE
    ),
    UTS_ENVIRONMENT => array(
      'name' => t('UTS Environment'),
      'module' => 'uts',
      'description' => t('Environment to be used as the basis for participants when' .
                         'starting a study.'),
      'body_label' => t('Description'),
      'custom' => TRUE,
      'locked' => TRUE
    )
  );
}

/**
 * Implementation of hook_form().
 */
function uts_form(&$node) {
  $form = array();
  $form['#submit'] = array();

  $type = node_get_types('type', $node);

  if ($type->has_title) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => $node->title,
      '#weight' => -10
    );
  }

  if ($type->has_body) {
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }

  if ($type->orig_type == UTS_STUDY) {
    if (isset($node->nid)) {
      // Don't bother displaying status when creating study.
      $form['study_status'] = array(
        '#type' => 'select',
        '#title' => t('Status'),
        '#description' => t('Mark the study active when it is ready for participants, and closed when it is
                             no longer open for further participation. If a maximum participant count is
                             specified then it will automatically be closed once the number has been reached.'),
        '#options' => array(
          UTS_STUDY_STATUS_IN_DEVELOPMENT,
          UTS_STUDY_STATUS_ACTIVE,
          UTS_STUDY_STATUS_CLOSED
        ),
        '#weight' => -9,
        '#default_value' => (isset($node->study_status) ? $node->study_status : ''),
      );
      $form['study_status']['#options'] = drupal_map_assoc($form['study_status']['#options'], 'uts_get_study_status');
    }
    else {
      // Set to default value.
      $form['study_status'] = array(
        '#type' => 'value',
        '#value' => UTS_STUDY_STATUS_IN_DEVELOPMENT
      );
    }

    $form['study_type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#description' => t('The type of study that is to be offered.'),
      '#options' => array(
        UTS_STUDY_TYPE_TASK
      ),
      '#weight' => -8,
      '#default_value' => (isset($node->study_type) ? $node->study_type : '')
    );
    $form['study_type']['#options'] = drupal_map_assoc($form['study_type']['#options'], 'uts_get_study_type');

    $form['participant_count'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of participants'),
      '#description' => t('The maximum number of participants that can participate in the study.'),
      '#size' => 4,
      '#maxlength' => 4,
      '#required' => TRUE,
      '#weight' => -7,
      '#default_value' => (isset($node->participant_count) ? $node->participant_count : '')
    );
    $form['body_field']['#weight'] = -6;
    $form['audience'] = array(
      '#type' => 'fieldset',
      '#title' => t('Audience'),
      '#description' => t('Define what type of participants are relevant for the study.'),
      '#weight' => -5
    );
    $form['audience']['audience_role'] = array(
      '#type' => 'select',
      '#title' => t('Role'),
      '#options' => array(
        UTS_STUDY_AUDIENCE_ROLE_USER,
        UTS_STUDY_AUDIENCE_ROLE_DESIGNER,
        UTS_STUDY_AUDIENCE_ROLE_PROGRAMER
      ),
      '#weight' => -10,
      '#default_value' => (isset($node->audience_role) ? $node->audience_role : '')
    );
    $form['audience']['audience_role']['#options'] = drupal_map_assoc($form['audience']['audience_role']['#options'], 'uts_get_study_audience_role');

    $form['audience']['audience_level'] = array(
      '#type' => 'select',
      '#title' => t('Experience level'),
      '#options' => array(
        UTS_STUDY_AUDIENCE_LEVEL_BEGINNER,
        UTS_STUDY_AUDIENCE_LEVEL_INTERMEDIATE,
        UTS_STUDY_AUDIENCE_LEVEL_ADVANCED
      ),
      '#weight' => -9,
      '#default_value' => (isset($node->audience_level) ? $node->audience_level : '')
    );
    $form['audience']['audience_level']['#options'] = drupal_map_assoc($form['audience']['audience_level']['#options'], 'uts_get_study_audience_level');

    $form['data_collection'] = array(
      '#type' => 'fieldset',
      '#title' => t('Data collection'),
      '#description' => t('Choose what data plug-ins to enable and which to require.'),
      '#weight' => -4,
      '#tree' => TRUE,
      '#theme' => 'uts_form_data_collection'
    );

    $plugins = uts_data_collection_list();
    foreach ($plugins as $module => $plugin) {
      $item = array();
      $item['enabled'] = array(
        '#type' => 'checkbox',
        '#default_value' => (isset($node->data_collection[$module]) ? TRUE : FALSE)
      );
      $item['required'] = array(
        '#type' => 'checkbox',
        '#default_value' => (isset($node->data_collection[$module]) && $node->data_collection[$module]['required'] ? TRUE : FALSE)
      );
      $item['name'] = array(
        '#type' => 'item',
        '#value' => $plugin['name']
      );
      $item['description'] = array(
        '#type' => 'item',
        '#value' => $plugin['description']
      );
      $item['software_required'] = array(
        '#type' => 'item',
        '#value' => $plugin['software_require']
      );
      $form['data_collection'][$module] = $item;
    }

    $form['base_environment'] = array(
      '#type' => 'fieldset',
      '#title' => t('Base environment'),
      '#description' => t('Manage the environment in which participants will begin their session. If no base environment is
                           created then a base Drupal installation will be used as the starting point.'),
      '#weight' => -3,
    );
    $form['base_environment']['base_environment'] = array(
      '#type' => 'select',
      '#title' => t('Environment'),
      '#description' => t('Choose from existing base environments.'),
      '#options' => array(0 => '--'),
      '#default_value' => (isset($node->base_environment) ? $node->base_environment : ''),
      '#weight' => -10
    );
    $environments = uts_environments_load();
    foreach ($environments as $environment) {
      $form['base_environment']['base_environment']['#options'][$environment->nid] = $environment->title;
    }
    $form['base_environment']['manage'] = array(
      '#type' => 'item',
      '#value' => l(t('Manage environments'), 'admin/uts/environments'),
      '#weight' => -9
    );
    $form['#redirect'] = 'admin/uts/studies';
  }
  else if ($type->orig_type == UTS_TASK) {
    if (is_numeric($study_nid = arg(3)) && arg(4) == 'add') {
      // Task will be added to a study.
      uts_set_title($study_nid);
    }
    else if (isset($node->study_nid) && $study_nid = $node->study_nid) {
      uts_set_title($study_nid);
    }
    else {
      unset($study_nid);
    }

    // Get list of studies for select.
    $studies = uts_studies_load();
    $list = array();
    $list[] = t('--');
    foreach ($studies as $study) {
      $list[$study->nid] = $study->title;
    }

    $form['study_nid'] = array(
      '#type' => 'select',
      '#title' => t('Parent study'),
      '#options' => $list,
      '#default_value' => (isset($study_nid) ? $study_nid : ''),
      '#weight' => -9.5
    );
    $form['body_field']['#weight'] = -9;
    $form['max_time'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum time'),
      '#description' => t('The maximum time a participant will be allowed for the task in seconds.'),
      '#size' => 6,
      '#maxlength' => 6,
      '#required' => TRUE,
      '#weight' => -8,
      '#default_value' => (isset($node->max_time) ? $node->max_time : '')
    );
    $form['#redirect'] = 'admin/uts/tasks/' . $form['study_nid']['#default_value'];
  }
  else if ($type->orig_type == UTS_ENVIRONMENT) {
    $form['prefix'] = array(
      '#type' => 'value',
      '#value' => (isset($node->environment_prefix) ? $node->environment_prefix : '')
    );
    $form['#redirect'] = 'admin/uts/environments';
  }

  return $form;
}

/**
 * Theme the data collection list into a table.
 *
 * @return string HTML output.
 */
function theme_uts_form_data_collection($data_collection) {
  $header = array(t('Enable'), t('Require'), t('Name'), t('Description'), t('Client software'));
  $rows = array();
  foreach (element_children($data_collection) as $module) {
    $plugin = &$data_collection[$module];
    $row = array();
    $row[] = drupal_render($plugin['enabled']);
    $row[] = drupal_render($plugin['required']);
    $row[] = drupal_render($plugin['name']);
    $row[] = drupal_render($plugin['description']);
    $row[] = ($plugin['software_required']['#value'] ? t('Required') : t('Not required'));

    $rows[] = $row;
  }
  if (empty($rows)) {
    return t('No data collection plug-ins enabled.');
  }
  return theme('table', $header, $rows);
}

/**
 * Implementation of hook_validate().
 */
function uts_validate($node, &$form) {
  $type = node_get_types('type', $node);

  if ($type->orig_type == UTS_STUDY) {
    if ($node->study_status == UTS_STUDY_STATUS_ACTIVE) {
      // Make sure the study has at least one task.
      if (!(isset($node->nid) && count(uts_tasks_load($node->nid)) > 0)) {
        form_set_error('study_status', t('The study must have at least one task to be considred active.'));
      }
    }
    if (!(is_numeric($node->participant_count) && (int) $node->participant_count == (float) $node->participant_count)) {
      form_set_error('participant_count', t('Participant count must be an integer.'));
    }
  }
  else if ($type->orig_type == UTS_TASK) {
    if (!(is_numeric($node->max_time) && (int) $node->max_time == (float) $node->max_time)) {
      form_set_error('max_time', t('Maximum time must be an integer.'));
    }
  }
}

/**
 * Implementation of hook_load().
 */
function uts_load($node) {
  $type = node_get_types('type', $node);

  if ($type->orig_type == UTS_STUDY) {
    // Load study data.
    $result = db_query('SELECT study_status, study_type, participant_count, audience_role, audience_level, base_environment
                        FROM {uts_study}
                        WHERE vid = %d',
                        $node->vid);
    $study = db_fetch_object($result);

    // Load data collection information.
    $result = db_query('SELECT name, required
                        FROM {uts_study_data}
                        WHERE study_nid = %d', $node->nid);
    $plugins = array();
    while ($plugin = db_fetch_object($result)) {
      $plugins[$plugin->name] = array('enabled' => TRUE, 'required' => (bool) $plugin->required);
    }
    $study->data_collection = $plugins;
    return $study;
  }
  else if ($type->orig_type == UTS_TASK) {
    // Load task data.
    $result = db_query('SELECT study_nid, max_time, weight
                        FROM {uts_task}
                        WHERE vid = %d',
                        $node->vid);
    return db_fetch_object($result);
  }
  else if ($type->orig_type == UTS_ENVIRONMENT) {
    // Load environment data.
    $result = db_query('SELECT prefix
                        FROM {uts_environment}
                        WHERE nid = %d',
                        $node->nid);
    return db_fetch_object($result);
  }
}

/**
 * Implementation of hook_insert().
 */
function uts_insert($node) {
  $type = node_get_types('type', $node);

  if ($type->orig_type == UTS_STUDY) {
    db_query('INSERT INTO {uts_study} (nid, vid, study_status, study_type, participant_count, audience_role, audience_level, base_environment)
              VALUES (%d, %d, %d, %d, %d, %d, %d, %d)',
              $node->nid, $node->vid, $node->study_status, $node->study_type, $node->participant_count,
              $node->audience_role, $node->audience_level, $node->base_environment);

    uts_update_data_collection($node);

    if (!$node->revision) {
      // First time saving study, display a message explaining what they may want to do next.
      if ($node->study_type == UTS_STUDY_TYPE_TASK) {
        drupal_set_message(t('You can now <a href="@tasks">add tasks</a> to your study.',
                              array('@tasks' => url('admin/uts/tasks/' . $node->nid . '/add'))));
      }
    }
  }
  else if ($type->orig_type == UTS_TASK) {
    db_query('INSERT INTO {uts_task} (nid, vid, max_time)
              VALUES (%d, %d, %d)',
              $node->nid, $node->vid, $node->max_time);
    if ($node->study_nid) {
      // Insert task into specific study.
      uts_tasks_import($node->study_nid, array($node->nid));
    }
  }
  else if ($type->orig_type == UTS_ENVIRONMENT) {
    if (!$node->revision) {
      // Create base environment for the first time.
      $node->prefix = 'utsbase' . mt_rand(1000, 1000000);
      uts_environment_invoke_callback('create', $node->prefix, 'base');
    }
    db_query("INSERT INTO {uts_environment} (nid, prefix)
              VALUES (%d, '%s')",
              $node->nid, $node->prefix);
  }
}

/**
 * Implementation of hook_update().
 */
function uts_update($node) {
  if ($node->revision) {
    uts_insert($node);
    return;
  }

  $type = node_get_types('type', $node);

  if ($type->orig_type == UTS_STUDY) {
    db_query('UPDATE {uts_study}
              SET study_status = %d,
                  study_type = %d,
                  participant_count = %d,
                  audience_role = %d,
                  audience_level = %d,
                  base_environment = %d
              WHERE vid = %d',
              $node->study_status, $node->study_type, $node->participant_count, $node->audience_role,
              $node->audience_level, $node->base_environment, $node->vid);

    uts_update_data_collection($node);
  }
  else if ($type->orig_type == UTS_TASK) {
    db_query('UPDATE {uts_task}
              SET max_time = %d
              WHERE vid = %d',
              $node->max_time, $node->vid);
  }
  else if ($type->orig_type == UTS_ENVIRONMENT) {
    // Once prefix has been created it cannot be changed.
  }
}

/**
 * Update data colleciton information for the specified node.
 *
 * @param object $node Node object.
 */
function uts_update_data_collection($node) {
  db_query('DELETE FROM {uts_study_data}
            WHERE study_nid = %d', $node->nid);

  if (isset($node->data_collection)) {
    // Check for no data collection modules enabled.
    foreach ($node->data_collection as $module => $state) {
      if ($state['enabled'] || $state['required']) {
        db_query("INSERT INTO {uts_study_data} (study_nid, name, required)
                  VALUES (%d, '%s', %d)", $node->nid, $module, $state['required']);
      }
    }
  }
}

/**
 * Implementation of hook_delete().
 */
function uts_delete($node) {
  $type = node_get_types('type', $node);

  if ($type->orig_type == UTS_STUDY) {
    // Delete all child tasks.
    $tasks = uts_tasks_load($node->nid);
    foreach ($tasks as $task) {
      node_delete($task->nid);
    }

    // Remove all data collected.
    uts_data_delete($node->nid);

    // Remove sessions related to study.
    uts_session_destroy_all($node->nid);

    // Remove study data.
    db_query('DELETE FROM {uts_study}
              WHERE nid = %d', $node->nid);
  }
  else if ($type->orig_type == UTS_TASK) {
    db_query('DELETE FROM {uts_task}
              WHERE nid = %d', $node->nid);
  }
  else if ($type->orig_type == UTS_ENVIRONMENT) {
    // Remove prefixed environment.
    uts_environment_invoke_callback('destroy', $node->prefix);

    // Remove base environment from any studies that may reference it.
    db_query('UPDATE {uts_study}
              SET base_environment = %d
              WHERE base_environment = %d', 0, $node->nid);

    db_query('DELETE FROM {uts_environment}
              WHERE nid = %d', $node->nid);
  }
}

/**
 * Implementation of hook_view().
 */
function uts_view($node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
  $type = node_get_types('type', $node);

  if ($type->orig_type == UTS_STUDY) {
    // Basic meta data.
    $node->content['study_status'] = array(
      '#value' => t('<b>Study status:</b> @status<br />', array('@status' => uts_get_study_status($node->study_status))),
      '#weight' => -10
    );
    $node->content['study_type'] = array(
      '#value' => t('<b>Study type:</b> @type<br />', array('@type' => uts_get_study_type($node->study_type))),
      '#weight' => -9
    );
    $node->content['audience'] = array(
      '#value' => t('<b>Audience:</b> @role (@level)<br />',
                    array('@role' => uts_get_study_audience_role($node->audience_role),
                           '@level' => uts_get_study_audience_level($node->audience_level))),
      '#weight' => -8
    );
    $node->content['participant_count'] = array(
      '#value' => t('<b>Number of participants:</b> @count<br />', array('@count' => $node->participant_count)),
      '#weight' => -7
    );
    if ($node->base_environment) {
      $base_environment = node_load($node->base_environment);
      $environment = l(t($base_environment->title), 'node/' . $base_environment->nid);
    }
    else {
      $environment = t('Default installation');
    }
    $node->content['base_environment'] = array(
      '#value' => t('<b>Base environment:</b> !environment<br />', array('!environment' => $environment)),
      '#weight' => -7
    );

    if (!$teaser) {
      // Related tasks.
      $tasks = uts_tasks_load($node->nid);
      $list = array();
      $max_time = 0;
      foreach ($tasks as $task) {
        $list[] = t('<b>!title</b><br />@body', array('!title' => l($task->title, 'node/' . $task->nid), '@body' => $task->body));
        $max_time += $task->max_time;
      }
      $node->content['tasks'] = array(
        '#value' => t('<b>Tasks:</b><br />!list', array('!list' => theme('item_list', $list))),
        '#weight' => 1
      );

      // Calculate max time.
      $node->content['max_time'] = array(
        '#value' => t('<b>Maximum time:</b> @time<br />', array('@time' => format_interval($max_time))),
        '#weight' => -6
      );

      // Data collectin plug-ins.
      $plugins = uts_data_collection_list();
      $list = array();
      foreach ($node->data_collection as $plugin => $state) {
        $list[] = '<b>' . $plugins[$plugin]['name'] . '</b>: ' . $plugins[$plugin]['description'];
      }
      $node->content['data_collection'] = array(
        '#value' => t('<b>Data colleciton plug-ins:</b><br />!list', array('!list' => theme('item_list', $list))),
        '#weight'=> 2
      );
    }
  }
  else if ($type->orig_type == UTS_TASK) {
    $node->content['max_time'] = array(
      '#value' => t('<b>Maximum time:</b> @time<br />', array('@time' => format_interval($node->max_time))),
      '#weight' => -10
    );
  }
  else if ($type->orig_type == UTS_ENVIRONMENT) {
    $node->content['prefix'] = array(
      '#value' => t('<b>Prefix:</b> @prefix<br />', array('@prefix' => $node->prefix)),
      '#weight' => -10
    );
  }
  return $node;
}

/**
 * Implementation of hook_link().
 */
function uts_link($type, $object, $teaser = FALSE) {
  $links = array();

  if (user_access('manage studies')) {
    if ($object->type == UTS_STUDY) {
      $links['add_task'] = array(
        'title' => t('Add task'),
        'href' => "admin/uts/tasks/$object->nid/add",
        'attributes' => array('title' => t('Add a task to this study.'))
      );
      $links['manage_tasks'] = array(
        'title' => t('Manage tasks'),
        'href' => "admin/uts/tasks/$object->nid",
        'attributes' => array('title' => t('Manage this study\'s tasks.'))
      );
      if (uts_session_load_all($object->nid, TRUE)) {
        $links['analyze_study'] = array(
          'title' => t('Analyze study'),
          'href' => "admin/uts/analyze/$object->nid",
          'attributes' => array('title' => t('Analyze the results of this study.'))
        );
      }
    }
    else if ($object->type == UTS_TASK) {
      $links['add_task'] = array(
        'title' => t('Add task'),
        'href' => "admin/uts/tasks/$object->study_nid/add",
        'attributes' => array('title' => t('Add a task to the study.'))
      );
      $links['manage_tasks'] = array(
        'title' => t('Manage tasks'),
        'href' => "admin/uts/tasks/$object->study_nid",
        'attributes' => array('title' => t('Manage the study\'s tasks.'))
      );
      $links['view_study'] = array(
        'title' => t('View study'),
        'href' => "node/$object->study_nid",
        'attributes' => array('title' => t('View the parent study.'))
      );
    }
    else if ($object->type == UTS_ENVIRONMENT) {
      $links['open_environment'] = array(
        'title' => t('Open environment'),
        'href' => "admin/uts/environments/$object->nid/open",
        'attributes' => array('title' => t('Open the environment to view or edit it.'))
      );
    }
  }

  return $links;
}
