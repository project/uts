<?php
/**
 * @file
 * Provide data analysis plug-in for the Usability Testing Suite.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_uts_data_analysis().
 */
function uts_path_uts_data_analysis() {
  return array(
    'uts_path' => array('list', 'compare')
  );
}

/**
 * Implementation of hook_uts_data_display().
 */
function uts_path_uts_data_display($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($module == 'uts_path' && $mode == 'list') {
    $data = uts_path_uts_data_get($study_nid, $session_id, $start_timestamp, $stop_timestamp);
    $rows = $data['uts_path'];

    // Format data.
    foreach ($rows as &$row) {
      $row['timestamp'] = format_date($row['timestamp']);
    }

    $header = array(t('Session id'), t('Timestamp'), t('Path'), t('Breadcrumb'), t('Title'));
    return array(
      array(
        '#data' => (empty($rows) ? t('<p>No path to display.</p>') : theme('table', $header, $rows)),
        '#weight' => -10
      )
    );
  }
  else if ($module == 'uts_path' && $mode == 'compare') {
    return array(
      array(
        '#data' => 'TODO<br />',
        '#weight' => -10
      )
    );
  }
}

/**
 * Implementation of hook_uts_data_export_analysis().
 */
function uts_path_uts_data_analysis_export($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($module == 'uts_path' && $mode == 'list') {
    $data = uts_path_uts_data_get($study_nid, $session_id, $start_timestamp, $stop_timestamp);
    $rows = $data['uts_path'];

    $out = '"' . implode('","', array(t('Session id'), t('Timestamp'), t('Path'), t('Breadcrumb'), t('Title'))) . "\"\n";
    foreach ($rows as $row) {
      $out .= '"' . implode('","', $row) . "\"\n";
    }
    return array(
      'uts_path.list.csv' => $out
    );
  }
  else if ($module == 'uts_path' && $mode == 'compare') {
    return array(
      'uts_path.compare.txt' => 'TODO'
    );
  }
}
