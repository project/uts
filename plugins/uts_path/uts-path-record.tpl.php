<?php
?>
<div class="uts-overlay">
  <div id="uts-status" class="uts-overlay-item">
    <?php print $status ?>
  </div>
  <div id="uts-actions" class="uts-overlay-item">
    <?php print $actions; ?>
  </div>
</div>