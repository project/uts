<?php
/**
 * @file
 * Provide data analysis plug-in for the Usability Testing Suite.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_uts_data_analysis().
 */
function uts_form_uts_data_analysis() {
  return array(
    'uts_form' => array('list')
  );
}

/**
 * Implementation of hook_uts_data_display().
 */
function uts_form_uts_data_display($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($module == 'uts_form' && $mode == 'list') {
    $data = uts_form_uts_data_get($study_nid, $session_id, $start_timestamp, $stop_timestamp);
    $rows = $data['uts_form'];

    // Format data.
    foreach ($rows as &$row) {
      $row['timestamp'] = format_date($row['timestamp']);
      $row['form_data'] = unserialize($row['form_data']);

      $row['form_data'] = highlight_string('<?php ' . var_export($row['form_data'], TRUE) . ' ?>', TRUE);
    }

    $header = array(t('Session id'), t('Timestamp'), t('Form ID'), t('Form Data'));
    return array(
      array(
        '#data' => (empty($rows) ? t('<p>No form submissions to display.</p>') : theme('table', $header, $rows)),
        '#weight' => -10
      )
    );
  }
}

/**
 * Implementation of hook_uts_data_export_analysis().
 */
function uts_form_uts_data_analysis_export($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($module == 'uts_form' && $mode == 'list') {
    $data = uts_form_uts_data_get($study_nid, $session_id, $start_timestamp, $stop_timestamp);
    $rows = $data['uts_form'];

    $out = '"' . implode('","', array(t('Session id'), t('Timestamp'), t('Form ID'), t('Form Data'))) . "\"\n";
    foreach ($rows as $row) {
      $out .= '"' . implode('","', $row) . "\"\n";
    }
    return array(
      'uts_form.list.csv' => $out
    );
  }
}
