
// Click toggle bar then toggle form display.
$('div#uts-live-feedback-toggle').click(function() {
  $('div#uts-live-feedback-form').slideToggle();
});
// On form submit clear feedback textarea.
function uts_response(id) {
  setTimeout("$('div#" + id + "').fadeOut(1000);", 19000);
  $('div#uts-live-feedback-form').find('textarea#edit-feedback').val('');
}
