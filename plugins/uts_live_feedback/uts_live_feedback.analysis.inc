<?php
/**
 * @file
 * Provide data analysis plug-in for the Usability Testing Suite.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_uts_data_analysis().
 */
function uts_live_feedback_uts_data_analysis() {
  return array(
    'uts_live_feedback' => array('list')
  );
}

/**
 * Implementation of hook_uts_data_display().
 */
function uts_live_feedback_uts_data_display($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($module == 'uts_live_feedback' && $mode == 'list') {
    $data = uts_live_feedback_uts_data_get($study_nid, $session_id, $start_timestamp, $stop_timestamp);
    $rows = $data['uts_live_feedback'];

    // Format data.
    foreach ($rows as &$row) {
      $row['timestamp'] = format_date($row['timestamp']);
      $row['message'] = nl2br($row['message']);
    }

    $header = array(t('Session id'), t('Timestamp'), t('Message'));
    return array(
      array(
        '#data' => (empty($rows) ? t('<p>No feedback to display.</p>') : theme('table', $header, $rows)),
        '#weight' => -10
      )
    );
  }
}

/**
 * Implementation of hook_uts_data_export_analysis().
 */
function uts_live_feedback_uts_data_analysis_export($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($module == 'uts_live_feedback' && $mode == 'list') {
    $data = uts_live_feedback_uts_data_get($study_nid, $session_id, $start_timestamp, $stop_timestamp);
    $rows = $data['uts_live_feedback'];

    $out = '"' . implode('","', array(t('Session id'), t('Timestamp'), t('Message'))) . "\"\n";
    foreach ($rows as $row) {
      $out .= '"' . implode('","', $row) . "\"\n";
    }
    return array(
      'uts_live_feedback.list.csv' => $out
    );
  }
}
