<?php
/**
 * @file
 * Collect user comments through an AJAX dialog while running a test
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

require_once drupal_get_path('module', 'uts_live_feedback') . '/uts_live_feedback.analysis.inc';
require_once drupal_get_path('module', 'uts_proctor') . '/uts_proctor.data.inc';

/**
 * Implementation of hook_uts_data_collection().
 */
function uts_live_feedback_uts_data_collection() {
  return array(
    'uts_live_feedback' => array(
      'name' => t('Live feedback'),
      'description' => t('Collect user comments through an AJAX dialog while running a test.'),
      'software_require' => FALSE,
      'javascript' => TRUE
    )
  );
}

/**
 * Implementation of hook_uts_client_requirements().
 */
function uts_live_feedback_uts_client_requirements() {
  return array(
    'uts_live_feedback' => array(
      'status' => 'ok',
      'name' => t('Live feedback'),
      'description' => t('All requirements met, data collection enabled.')
    )
  );
}

/**
 * Implementation of hook_uts_data_get().
 */
function uts_live_feedback_uts_data_get($study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($session_id && $start_timestamp && $stop_timestamp) {
    $result = db_query("SELECT lt.session_id, lt.timestamp, lt.message
                        FROM {uts_live_feedback} lt, {uts_session} s
                        WHERE lt.session_id = s.session_id
                        AND s.study_nid = %d
                        AND lt.timestamp >= %d
                        AND lt.timestamp <= %d
                        AND lt.session_id = '%s'
                        ORDER BY timestamp ASC", $study_nid, $start_timestamp, $stop_timestamp, $session_id);
  }
  else {
    $result = db_query("SELECT lt.session_id, lt.timestamp, lt.message
                        FROM {uts_live_feedback} lt, {uts_session} s
                        WHERE lt.session_id = s.session_id
                        AND s.study_nid = %d
                        ORDER BY timestamp ASC", $study_nid);
  }
  $data = array();
  while ($record = db_fetch_array($result)) {
    $data[] = $record;
  }
  return array('uts_live_feedback' => $data);
}

/**
 * Implementation of hook_uts_data_delete().
 */
function uts_live_feedback_uts_data_delete($study_nid, $session_id = NULL) {
  if ($session_id) {
    db_query("DELETE FROM {uts_live_feedback}
              WHERE session_id = '%s'", $session_id);
  }
  else {
    $sessions = uts_session_load_all($study_nid);
    if (count($sessions) > 0) {
      $placeholders = implode("','", array_fill(0, count($sessions), '%s'));
      db_query("DELETE FROM {uts_live_feedback}
                WHERE session_id IN ('$placeholders')", $sessions);
    }
  }
}

/**
 * Implementation of hook_menu().
 */
function uts_live_feedback_menu() {
  $items = array();
  $items['uts/data/live_feedback'] = array(
    'title' => 'Collect live feedback',
    'description' => 'Record form submission from user.',
    'page callback' => 'uts_live_feedback_record',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK
  );
  return $items;
}

/**
 * Implementation of hook_theme().
 */
function uts_live_feedback_theme() {
  return array(
    'uts_live_feedback_feedback' => array(
      'arguments' => array('form' => NULL),
      'template' => 'uts-live-feedback'
    )
  );
}

/**
 * Implementation of hook_init().
 *
 * Output feedback interface and include stylesheet.
 */
function uts_live_feedback_init() {
  global $uts_session;

  if (uts_proctor_data_record()) {
    drupal_add_css(drupal_get_path('module', 'uts_live_feedback') . '/uts-live-feedback.css');
    drupal_add_js(drupal_get_path('module', 'uts_live_feedback') . '/uts_live_feedback.js', 'module', 'footer');
    drupal_set_content('footer', theme('uts_live_feedback_feedback', drupal_get_form('uts_live_feedback_feedback_form')));
  }
}

/**
 * Menu callback: Output result of feedback submission.
 */
function uts_live_feedback_record() {
  global $uts_session;

  $errors = form_get_errors();
  drupal_get_messages(); // Don't display error messages on next page call.
  if (isset($errors['feedback'])) {
    drupal_json(uts_live_feedback_fade_message($errors['feedback']));
  }
  else {
    $result = uts_proctor_call_uts('db_query', "SELECT message
                                                FROM {uts_live_feedback}
                                                WHERE session_id = '%s'
                                                ORDER BY timestamp DESC
                                                LIMIT 1", $uts_session['session_id']);
    drupal_json(uts_live_feedback_fade_message('"' . db_result($result) . '"'));
  }
}

/**
 * Add JavaScript to message so that it will fade after set amount of time.
 *
 * @param string $message Message to fade.
 * @return string HTML output.
 */
function uts_live_feedback_fade_message($message) {
  $id = mt_rand(100, 100000);
  return t('<div id="@id">!message<script>uts_response(@id)</script></div>',
            array('@id' => $id, '!message' => nl2br(check_plain($message))));
}

/**
 * Feedback form.
 */
function uts_live_feedback_feedback_form(&$form_state) {
  $form = array();
  $form['feedback'] = array(
    '#type' => 'textarea',
    '#title' => t('Feedback'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    '#ahah' => array(
      'path' => 'uts/data/live_feedback',
      'wrapper' => 'uts-live-feedback-result',
      'method' => 'append',
      'effect' => 'fade'
    )
  );

  return $form;
}

function uts_live_feedback_feedback_form_validate($form, &$form_state) {
  if (trim($form_state['values']['feedback']) == '') {
    form_set_error('feedback', t('Must enter feedback.'));
  }
}

/**
 * Feedback form submit handler: Save feedback record.
 */
function uts_live_feedback_feedback_form_submit($form, &$form_state) {
  global $uts_session;

  uts_proctor_call_uts('db_query',
                       "INSERT INTO {uts_live_feedback}
                        (session_id, timestamp, message)
                        VALUES ('%s', %d, '%s')",
                        $uts_session['session_id'], uts_proctor_current_timestamp(), trim($form_state['values']['feedback']));
}
