<?php
?>
<div id="uts-live-feedback">
  <div id="uts-live-feedback-toggle">
    Feedback
  </div>
  <div id="uts-live-feedback-form">
    <div id="uts-live-feedback-result"></div>
    <?php print $form; ?>
  </div>
</div>
