<?php
/**
 * @file
 * Provide Usability Testing Suite session functions.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Generate session id to be used for database and files directory prefix,
 * create Drupal environment, store session record, and create UTS session
 * cookie used to load testing environment.
 *
 * @param integer $study_nid Study NID to create a session for.
 * @return mixed Session ID of created session or FALSE on failure.
 */
function uts_session_create($study_nid) {
  // Generate session id.
  $session_id = 'uts' . mt_rand(1000, 1000000);

  // Generate test environment, both database and files directory.
  $study = node_load($study_nid);
  $copy_prefix = NULL;
  if ($study->base_environment) {
    $copy_prefix = node_load($study->base_environment);
    $copy_prefix = $copy_prefix->prefix;
  }
  if (!uts_environment_invoke_callback('create', $session_id, $copy_prefix)) {
    return FALSE;
  }

  // Load the tasks related to the study and select the first one for the current task.
  $tasks = uts_tasks_load($study_nid);
  db_query("INSERT INTO {uts_session}
            (session_id, study_nid, current_task)
            VALUES ('%s', %d, %d)", $session_id, $study_nid, $tasks[0]->nid);

  // Create session cookie.
  uts_session_set_cookie($session_id);

  return $session_id;
}

/**
 * Restore existing session.
 *
 * @param string $session_id Session ID.
 */
function uts_session_restore($session_id) {
  // Ensure that cookie exits and set it to active.
  uts_session_set_cookie($session_id, TRUE);
}

/**
 * Check for an active user session via cookies.
 *
 * @return Active user session.
 */
function uts_session_active() {
  return isset($_COOKIE['uts_session']) &&
         isset($_COOKIE['uts_session_active']) && $_COOKIE['uts_session_active'];
}

/**
 * Load UTS session data from database.
 *
 * @param string $session_id Session ID.
 * @return object Session object.
 */
function uts_session_load($session_id) {
  $result = db_query("SELECT session_id, study_nid, complete, current_task, start_time
                      FROM {uts_session}
                      WHERE session_id = '%s'", $session_id);
  return db_fetch_object($result);
}

/**
 * Update session task or start time.
 *
 * @param string $session_id The session ID of the session to update.
 * @param interger $task_id The task ID to update session to and set start time to NULL.
 * @param boolean $start Update the start time instead of the task ID.
 */
function uts_session_update($session_id, $task_id = NULL, $start = FALSE) {
  if ($start) {
    db_query("UPDATE {uts_session}
              SET start_time = %d
              WHERE session_id = '%s'", time(), $session_id);
  }
  else {
    db_query("UPDATE {uts_session}
              SET current_task = %d,
              start_time = NULL
              WHERE session_id = '%s'", $task_id, $session_id);
  }
}

/**
 * Mark a session as completed, null the start time, and destroy
 * the test environment.
 *
 * @param string $session_id Session ID to close.
 * @return boolean Success.
 */
function uts_session_close($session_id) {
  db_query("UPDATE {uts_session}
            SET complete = %d,
            start_time = NULL
            WHERE session_id = '%s'", TRUE, $session_id);
  return TRUE;
}

/**
 * Destroy testing environment, remove client cookie, and delete session
 * record from database.
 *
 * @param string $session_id Session to destory.
 * @return boolean Success.
 */
function uts_session_destory($session_id) {
  if (!uts_environment_invoke_callback('destroy', $session_id)) {
    return FALSE;
  }
  uts_session_set_cookie(FALSE);

  // Delete all data stored for session by data collection plug-ins.
  uts_data_delete(NULL, $session_id);

  db_query("DELETE FROM {uts_session_task}
            WHERE session_id = '%s'", $session_id);
  db_query("DELETE FROM {uts_session}
            WHERE session_id = '%s'", $session_id);
  return TRUE;
}

/**
 * Destroy all sessions or sessions related to a specified study NID.
 *
 * @param string $study_nid Study NID of sessions to remove, if NULL then
 *   all sessions will be destroyed.
 * @return boolean Success.
 */
function uts_session_destroy_all($study_nid = NULL) {
  $sessions = uts_session_load_all($study_nid);

  $success = TRUE;
  foreach($sessions as $session_id) {
    if (!uts_session_destory($session_id)) {
      $success = FALSE;
    }
  }
  return $success;
}

/**
 * Load all session IDs.
 *
 * @param string $study_nid Study NID of sessions to remove, if NULL then
 *   all sessions will be destroyed.
 * @param boolean $complete Completion status.
 * @return array List of session IDs.
 */
function uts_session_load_all($study_nid = NULL, $complete = NULL) {
  if ($study_nid) {
    if ($complete !== NULL) {
      $result = db_query('SELECT session_id
                          FROM {uts_session}
                          WHERE study_nid = %d
                          AND complete = %d
                          ORDER BY session_id ASC', $study_nid, $complete);
    }
    else {
      $result = db_query('SELECT session_id
                          FROM {uts_session}
                          WHERE study_nid = %d
                          ORDER BY session_id ASC', $study_nid);
    }
  }
  else {
    if ($complete !== NULL) {
      $result = db_query('SELECT session_id
                        FROM {uts_session}
                        WHERE complete = %d
                        ORDER BY session_id ASC', $complete);
    }
    else {
      $result = db_query('SELECT session_id
                          FROM {uts_session}
                          ORDER BY session_id ASC');
    }
  }

  $sessions = array();
  while ($session_id = db_result($result)) {
    $sessions[] = $session_id;
  }
  return $sessions;
}

/**
 * Set the UTS session cookie.
 *
 * @param string $session_id Session ID.
 * @param boolean $active Session is active.
 */
function uts_session_set_cookie($session_id, $active = FALSE) {
  // Save session for a modest 14 days, afterwhich the session cookie can be
  // re-created. Cookie signifying active session dies when browser is closed.
  setcookie('uts_session', $session_id, time() + 3600 * 24 * 14, '/');
  setcookie('uts_session_active', ($active ? '1' : '0'), 0, '/');
}
