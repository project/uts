
/**
 * Add collapse/expand triggers to the dashboard rows.
 *
 * @see Modified from Drupal 7 core simpletest.js.
 */
Drupal.behaviors.utsMenuCollapse = function() {
   var timeout = null;

   // Adds expand-collapse functionality.
   $('div.uts-image').each(function() {
     direction = Drupal.settings.uts[$(this).attr('id')].imageDirection;
     $(this).html(Drupal.settings.uts.images[direction]);
   });

   // Adds group toggling functionality to arrow images.
   $('div.uts-image').click(function() {
     var trs = $(this).parents('tbody').children('.' + this.id + '-task');
     var direction = Drupal.settings.uts[this.id].imageDirection;
     var row = direction ? trs.size() - 1 : 0;

     // If clicked in the middle of expanding a group, stop so we can switch directions.
     if (timeout) {
       clearTimeout(timeout);
     }

     // Function to toggle an individual row according to the current direction.
     // We set a timeout of 20 ms until the next row will be shown/hidden to
     // create a sliding effect.
     function rowToggle() {
       if (direction) {
         if (row >= 0) {
           $(trs[row]).hide();
           row--;
           timeout = setTimeout(rowToggle, 20);
         }
       }
       else {
         if (row < trs.size()) {
           $(trs[row]).removeClass('js-hide').show();
           row++;
           timeout = setTimeout(rowToggle, 20);
         }
       }
     }

     // Kick-off the toggling upon a new click.
     rowToggle();

     // Toggle the arrow image next to the test group title.
     $(this).html(Drupal.settings.uts.images[(direction ? 0 : 1)]);
     Drupal.settings.uts[this.id].imageDirection = !direction;
   });
};

/**
 * Expand/Collapse the task details on the analysis page.
 */
Drupal.behaviors.utsTaskAccordion = function() {
  $('div.uts-task-open').each(function() {
    $(this).children('span').html(Drupal.settings.uts.images[0]);
  });

  $('div.uts-task-open').click(function() {
    var task = $(this).attr('id').replace('uts-task-open-', '');
    if ($(this).children('span').children('img:first').attr('title') == $(Drupal.settings.uts.images[0]).attr('title')) {
      // Expand details.
      $(this).children('span').html(Drupal.settings.uts.images[1]);
      $('div#uts-task-' + task).parent().parent().css('display', '');
      $('div#uts-task-' + task).show('fast');
    }
    else {
      // Collapse details.
      $(this).children('span').html(Drupal.settings.uts.images[0]);
      $('div#uts-task-' + task).hide('fast', function() { $('div#uts-task-' + task).parent().parent().css('display', 'none'); });
    }
  });
};

/**
 * Show/Hide the participant comments.
 */
Drupal.behaviors.utsLiveFeedback = function() {
  $('div.uts-live-feedback-participant').click(function() {
    // Fade other participant buttons.
    $(this).parent().children('div.uts-live-feedback-participant').fadeTo('fast', 0.40);

    // Focus current participant button.
    $(this).fadeTo('fast', 1.0);

    $parent = $(this).parent().parent();

    // Hide all other comments.
    $parent.children('div.uts-live-feedback-comments').hide('fast');

    // Show the comments that relate to the specified participant.
    $parent.children('div#uts-comments-' + $(this).text()).show('fast');

    // Create accordion if the comments are being viewed for the first time.
    $ul = $parent.children('div#uts-comments-' + $(this).text()).find('ul');
    if (!$ul.hasClass('ui-accordion')) {
      $parent.children('div#uts-comments-' + $(this).text()).find('ul').accordion();
    }
  });
};
