<?php
/**
 * @file
 * Provide particpation batch definitions.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Batch to create an environment.
 *
 * @param integer $study_nid Study NID.
 */
function uts_participate_environment_batch($study_nid) {
  // Validate, create, e-mail, enable modules load session.
  $batch = array(
    'operations' => array(
      array('uts_participate_environment_batch_session', array($study_nid)),
      array('uts_participate_environment_batch_notify', array($study_nid)),
      array('uts_participate_environment_batch_close', array($study_nid)),
    ),
    'finished' => 'uts_participate_environment_batch_finished',
    'title' => t('Create a session and an environment'),
    'init_message' => t('Creating environment...'),
    'error_message' => t('An error occured during environment setup.'),
  );

  batch_set($batch);
}

/**
 * Create a session and an environment.
 *
 * @param integer $study_nid Study NID.
 */
function uts_participate_environment_batch_session($study_nid, &$context) {
  $session_id = uts_session_create($study_nid);

  $context['message'] = t('Environment created.');
  $context['results']['session_id'] = $session_id;
}

/**
 * Send e-mail containing session information.
 *
 * @param integer $study_nid Study NID.
 */
function uts_participate_environment_batch_notify($study_nid, &$context) {
  $session_id = $context['results']['session_id'];

  if ($session_id) {
    global $user;
    $params = array();
    $params['account'] = $user;
    $params['study_nid'] = $study_nid;
    $params['session_id'] = $session_id;
    drupal_mail('uts', 'session_info', $user->mail, user_preferred_language($user), $params);

    $context['message'] = t('E-mail sent reguarding environment information.');
  }
}

/**
 * Check the number of existing sessions against the maximum participant count.
 * If maximum count reached then close study.
 *
 * @param integer $study_nid Study NID.
 */
function uts_participate_environment_batch_close($study_nid, &$context) {
  $study = node_load($study_nid);
  $sessions = uts_session_load_all($study->nid);
  if (count($sessions) >= $study->participant_count) {
    $study->study_status = UTS_STUDY_STATUS_CLOSED;
    $study->revision = FALSE;
    $study = node_submit($study);
    node_save($study);
    $context['message'] = t('Maximum number of sessions reached, study closed.');
  }
}

/**
 * Batch 'finished' callback.
 */
function uts_participate_environment_batch_finished($success, $results, $operations) {
  if ($success) {
    if ($results['session_id']) {
      drupal_set_message(t('Environment created successfully.'));
      drupal_goto('uts/session/' . $results['session_id']);
    }
    else {
      drupal_set_message(t('Failed to setup environment. Please contact the administrator.'), 'error');
    }
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message('An error occurred while processing ' . $error_operation[0] . ' with arguments :' .
                        print_r($error_operation[0], TRUE));
  }
}

/**
 * Setup session.
 *
 * @param string $session_id UTS session ID.
 * @param array $modules Associative array of data collection module and status.
 */
function uts_participate_session_batch($session_id, $modules) {
  $batch = array(
    'operations' => array(
      array('uts_participate_session_batch_modules', array($session_id, $modules)),
    ),
    'finished' => 'uts_participate_session_batch_finished',
    'title' => t('Start session'),
    'init_message' => t('Configuring data plug-ins...'),
    'error_message' => t('An error occured during session setup.'),
  );

  batch_set($batch);
}

/**
 * Update data collection plug-ins in testing environment.
 *
 * @param string $session_id UTS session ID.
 * @param array $modules Associative array of data collection module and status.
 */
function uts_participate_session_batch_modules($session_id, $modules, &$context) {
  $enable = array();
  $disable = array();
  foreach ($modules as $module => $status) {
    if ($status != 'error') {
      $enable[] = $module;
    }
    else {
      $disable[] = $module;
    }
  }
  // Enable to proctor to ensure that menu hooks are added appropriately.
  $disable[] = 'uts_proctor'; // TODO If not disabled then menu hooks are not picked up for some reason.
  $enable[] = 'uts_proctor';

  if (!empty($disable)) {
    uts_environment_invoke_callback('disable_modules', $session_id, implode(',', $disable));
  }
  if (!empty($enable)) {
    uts_environment_invoke_callback('enable_modules', $session_id, implode(',', $enable));
  }
  $context['results']['session_id'] = $session_id;
  $context['message'] = t('Data collection plug-ins configured.');
}

/**
 * Batch 'finished' callback.
 */
function uts_participate_session_batch_finished($success, $results, $operations) {
  if ($success) {
    // Should be inside session and will not see messages anyway.
    uts_session_restore($results['session_id']);
    drupal_goto('');
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message('An error occurred while processing ' . $error_operation[0] . ' with arguments :' .
                        print_r($error_operation[0], TRUE));
  }
}
