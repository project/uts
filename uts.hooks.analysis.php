<?php
/**
 * @file
 * Provide data analysis hook documentation for Usability Testing Suite.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Get a list of data analysis plug-ins.
 *
 * @return array List of data analysis plug-ins.
 */
/**
 * Define data analysis modes the module provides. This provides the modes of
 * analysis that the module provides. The mode will be passed to subsequent
 * analysis functions in order to request a particular analysis.
 *
 * @return array Data analysis plug-in modes.
 *
 *   The array should use the module name as a key and contain an array of
 *   analysis modes that are provided.
 */
function hook_uts_data_analysis() {
  return array(
    'uts_plugin' => array('list')
  );
}

/**
 * Generate an alysis of data to be displayed.
 *
 * @param string $module Data collection module from which data will be retrieved.
 * @param string $mode The analysis mode.
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @param interger $start_timestamp Start timestamp.
 * @param integer $stop_timestamp Stop timestamp.
 * @return array Generated analysis elements.
 *
 *   The analysis is returned in the same format as hook_view(). Each analysis
 *   piece to display is returned as an array containing keys: #data, and
 *   #weight. This will allow multiple analaysis modules to add pieces of
 *   analysis to a particular mode.
 */
function hook_uts_data_display($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($module == 'uts_plugin' && $mode == 'list') {
    $data = uts_plugin_uts_data_get($study_nid, $session_id, $start_timestamp, $stop_timestamp);
    $rows = $data['uts_plugin'];

    // Format data.
    foreach ($rows as &$row) {
      $row['timestamp'] = format_date($row['timestamp']);
    }

    $header = array(t('Session id'), t('Timestamp'), t('Element 1'), t('Element 2'));
    return array(
      array(
        '#data' => (empty($rows) ? t('<p>No data to display.</p>') : theme('table', $header, $rows)),
        '#weight' => -10
      )
    );
  }
}

/**
 * Export the analysis for use in external programs, such as presentations. The
 * exported analysis will be compressed as a ZIP archive so that multiple files
 * can be included easily.
 *
 * @param string $module Data collection module from which data will be retrieved.
 * @param string $mode The analysis mode.
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @param interger $start_timestamp Start timestamp.
 * @param integer $stop_timestamp Stop timestamp.
 * @return array Exported analysis.
 *
 *   The exported analysis should be returned as a key-value array of filenames
 *   and data. The array will then be used to generate a ZIP archive and
 *   transfered to the user.
 */
function hook_uts_data_analysis_export($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  if ($module == 'uts_plugin' && $mode == 'list') {
    $data = uts_form_uts_data_get($study_nid, $session_id, $start_timestamp, $stop_timestamp);
    $rows = $data['uts_plugin'];

    $out = '"' . implode('","', array(t('Session id'), t('Timestamp'), t('Element 1'), t('Element 2'))) . "\"\n";
    foreach ($rows as $row) {
      $out .= '"' . implode('","', $row) . "\"\n";
    }
    return array(
      'uts_plugin.list.csv' => $out
    );
  }
}
