<?php
/**
 * @file
 * Provide interface pages.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Display list of active studies to choose from.
 *
 * @return string HTML output.
 */
function uts_participate_list() {
  $out = '<p>' . t('Thanks for participating in our studies for making Drupal better. Please follow the steps.') . '</p>';

  $studies = array_merge(uts_studies_load(UTS_STUDY_STATUS_ACTIVE));
  foreach ($studies as $study) {
    $out .= '<div class="uts-choose-study">';
    $out .= l($study->title, 'uts/environment/' . $study->nid);
    $teaser = node_teaser($study->body, NULL, 300);
    $out .= '<p>' . $study->body . '</p>';
    $out .= '</div>';
  }

  if (!$studies) {
    $out = '<p>' . t('No active studies.') . '</p>';
  }

  return $out;
}

/**
 * Create an environment for the session.
 *
 * @param integer $study_nid Study NID to display.
 */
function uts_participate_create_environment_form(&$form_state, $study_nid) {
  $study = node_load($study_nid);

  $form = array();
  $form['#validate'][] = 'uts_participate_create_environment_form_validate';
  $form['study_nid'] = array(
    '#type' => 'value',
    '#value' => $study_nid
  );
  $form['instructions'] = array(
    '#type' => 'item',
    '#value' => t('In order to participate you must create an environment for your session.')
  );

  $form += uts_participate_requirements($study);

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Create')
  );

  return $form;
}

/**
 * Make sure that there is still room in the study for another paritipant.
 */
function uts_participate_create_environment_form_validate($form, &$form_state) {
  // Check the number of existing sessions against the maximum participant count.
  $study = node_load($form_state['values']['study_nid']);
  $sessions = uts_session_load_all($study->nid);
  if (count($sessions) >= $study->participant_count) {
    form_set_error('study_nid', t('The study is no longer open to new participants. Please try a different study.'));
    drupal_goto('uts');
  }
}

/**
 * Start the batch process to create the session environment.
 */
function uts_participate_create_environment_form_submit($form, &$form_state) {
  uts_participate_environment_batch($form_state['values']['study_nid']);
}

/**
 * Start a session.
 *
 * @param string $session_id UTS session ID.
 */
function uts_participate_start_session_form(&$form_state, $session_id) {
  $session = uts_session_load($session_id);

  if (!$session) {
    drupal_set_message(t('Invalid session ID.'), 'error');
    drupal_goto('uts');
  }

  $study = node_load($session->study_nid);

  $form = array();
  $form['session_id'] = array(
    '#type' => 'value',
    '#value' => $session->session_id
  );
  $form['instructions'] = array(
    '#type' => 'item',
    '#value' => t('Start/Restore your session in which you are participating in %study.', array('%study' => $study->title))
  );

  $form += uts_participate_requirements($study);

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Start')
  );

  return $form;
}

/**
 * Start the batch process to configure the session.
 */
function uts_participate_start_session_form_submit($form, &$form_state) {
  uts_participate_session_batch($form_state['values']['session_id'], $form_state['values']['data_collection']);
}

/**
 * Create form elements for the data plug-in requirements.
 *
 * @param object $study Study object.
 * @return array Addition to root of form definition array.
 */
function uts_participate_requirements($study) {
  $form = array();
  $plugins = uts_data_collection_list();
  $requirements = uts_data_client_requirements();

  $form['#validate'][] = 'uts_participate_requirements_validate';

  // Check for javascript requirement.
  foreach ($plugins as $module => $plugin) {
    if ($plugin['javascript'] && $study->data_collection[$module]['required']) {
      $form['requirements']['javascript_status'] = array(
        '#type' => 'hidden',
        '#default_value' => 'not-found'
      );
      $form['requirements'][] = array(
        '#type' => 'markup',
        '#value' => '<div class="uts-requirement error" id="uts-javascript">' .
                      t('Javascript is not enabled. Please turn it on.') .
                    '</div>'
      );
      drupal_add_js('$("div#uts-javascript").hide(); $("input#edit-javascript-status").attr("value", "found");', 'inline', 'footer');
      break;
    }
  }

  $form['data_collection'] = array(
    '#type' => 'value',
    '#tree' => TRUE
  );
  foreach ($requirements as $module => $requirement) {
    if (array_key_exists($module, $study->data_collection)) {
      if ($study->data_collection[$module]['required'] &&
          $requirement['status'] != 'ok') {
        $form['requirements'][] = array(
          '#type' => 'markup',
          '#value' => '<div class="uts-requirement ' . $requirement['status'] . '" id="' . $module . '">' .
                        $requirement['description'] .
                      '</div>'
        );
      }
      $form['data_collection'][$module] = array(
        '#type' => 'value',
        '#value' => $requirement['status']
      );
    }
  }

  return $form;
}

/**
 * Ensure that all the requirements have been met.
 */
function uts_participate_requirements_validate($form, &$form_state) {
  // Check JavaScript requirement.
  if (isset($form_state['values']['javascript_status']) && $form_state['values']['javascript_status'] != 'found') {
    form_set_error('javascript_status', t('Javascript must be enabled.'));
  }

  // Check data plug-in requirements.
  $requirements = uts_data_client_requirements();
  foreach ($requirements as $module => $requirement) {
    foreach ($form_state['values']['data_collection'] as $module => $status) {
      if ($status != 'ok') {
        form_set_error($module . '_status', $requirement['description']);
      }
    }
  }
}

/**
 * Landing page after a participant completes a study.
 *
 * @param boolean $return Remind participant to return in order to finish their study.
 */
function uts_participate_thanks($return = FALSE) {
  if ($return) {
    drupal_set_message(t('Remember to come back and finish your study.'));
  }
  return t('<p>We appreciate you taking the time to participate in our study.</p>
            <p>Have a nice day.</p>');
}

/**
 * Dashboard displayed on admin/uts as the main landing page. If no studies
 * displays instructional video(s), otherwise summary view.
 *
 * @return string HTML output.
 */
function uts_dashboard() {
  drupal_add_css(drupal_get_path('module', 'uts') . '/uts.css', 'module');
  $studies = uts_studies_load();

  $out = '';
  $out .= '<div style="float: right"><a href="/admin/uts/studies/add" style="border: 1px solid; padding: 10px;">' . t('Create study') . '</a></div>';
  if (!$studies) {
    $out .= t('<p>Running a usability test will give you valuable data to improve your application.
                  Getting feedback from real people using your module is the best feedback you can get.
                  The usability testing suite allows you to easily collect exactly that data!</p>');
    $out .= '<div class="uts-dashboard">';
    $out .= l(t('Create study'), 'admin/uts/studies/add', array('attributes' => array('id' => 'uts-create-study')));
    $out .= '<h3 id="uts-learn">' . t('Learn about UTS, watch a video.') . '</h3>';
    $out .= '<embed src="http://blip.tv/play/AaXwG4mSHQ" type="application/x-shockwave-flash" width="640" height="510" allowscriptaccess="always" allowfullscreen="true"></embed>';
    $out .= '</div>';
  }
  else {
    $out .= '<div id="uts-dashboard-summary">' . uts_dashboard_studies($studies) . '</div>';
  }

  return $out;
}

/**
 * Display the summary of studies and participant progress.
 *
 * @param array $studies List of studies.
 * @return string HTML output.
 */
function uts_dashboard_studies($studies) {
  $header = array(t('Name'), t('Participants'), t('Task progress'));
  $rows = array();

  // Cycle through studies to generate summary table.
  foreach ($studies as $study) {
    $progress = uts_dashboard_studies_progress($study);
    $tasks = uts_tasks_load($study->nid);

    $row = array();
    $row[] = array(
      'data' => l($study->title, 'node/' . $study->nid),
      'id' => $study->nid
    );
    $row[] = t('@count / @required', array('@count' => $progress['overal']['participated'], '@required' => $study->participant_count));
    $row[] = theme('uts_progress', $progress['overal']['completed'], count($tasks) * $study->participant_count);

    $rows[] = $row;

    // Generate individual task rows.
    foreach ($tasks as $task) {
      $row = array();
      $row[]['data'] = l($task->title, 'node/' . $task->nid);
      $row[] = t('@count / @required', array('@count' => $progress[$task->nid]['participated'], '@required' => $study->participant_count));
      $row[] = theme('uts_progress', $progress[$task->nid]['completed'], $study->participant_count);

      $rows[] = $row;
    }
  }

  return theme('uts_dashboard_studies', $header, $rows);
}

/**
 * Add collapse/expand JavaScript to table rows.
 *
 * @return string HTML output.
 */
function theme_uts_dashboard_studies($header, $rows) {
  drupal_add_js(drupal_get_path('module', 'uts') . '/uts.js');

  $js = array(
    'images' => array(
       theme('image', 'misc/menu-collapsed.png', 'Expand', 'Expand'),
       theme('image', 'misc/menu-expanded.png', 'Collapsed', 'Collapsed'),
    ),
  );

  foreach ($rows as $key => $row) {
    $name = $rows[$key][0]['data'];
    if ($id = $rows[$key][0]['id']) {
      $last_study_id = 'uts-study-' . $id;
      $rows[$key][0] = array(
        'data'  => '<div class="uts-image" id="' . $last_study_id . '"></div>' . $name,
        'class' => 'uts-study',
        'style' => 'font-weight: bold;'
      );
      $js[$last_study_id] = array(
        'imageDirection' => 0 // Collapsed.
      );
    }
    else {
      $rows[$key][0] = theme('indentation', 1) . $name;
      $rows[$key] = array(
        'data' => $rows[$key],
        'class' => $last_study_id . '-task uts-task',
        'style' => 'display: none;'
      );
    }
  }
  drupal_add_js(array('uts' => $js), 'setting');
  return theme('table', $header, $rows);
}

/**
 * Calculate the progress made overal and per task for the specified study.
 *
 * @param object $study Study object.
 * @return array Progress overal and per task.
 */
function uts_dashboard_studies_progress($study) {
  $sessions = uts_session_load_all($study->nid);

  // Get the number of sessions that completed each task.
  $tasks = uts_tasks_load($study->nid);

  // Initialize all progress results to 0.
  $progress = array();
  $progress['overal']['completed'] = 0;
  $progress['overal']['participated'] = count($sessions);
  foreach ($tasks as $task) {
    $progress[$task->nid]['completed'] = 0;
    $progress[$task->nid]['participated'] = 0;
  }

  // Collect task progress data based on the related sessions.
  foreach ($sessions as $session_id) {
    $session = uts_session_load($session_id);

    if ($session->complete) {
      // Session completed all tasks.
      foreach ($tasks as $task) {
        $progress['overal']['completed']++;
        $progress[$task->nid]['completed']++;
        $progress[$task->nid]['participated']++;
      }
    }
    else {
      // See how far the session has progressed.
      foreach ($tasks as $task) {
        if ($session->current_task != $task->nid) {
          // Made it past this task.
          $progress['overal']['completed']++;
          $progress[$task->nid]['completed']++;
          $progress[$task->nid]['participated']++;
        }
        else {
          $progress[$task->nid]['participated']++;
          break;
        }
      }
    }
  }
  return $progress;
}

/**
 * Theme a progress bar.
 *
 * @param integer $progress Progress part out of total.
 * @param integer $total Total required.
 * @return string HTML output.
 */
function theme_uts_progress($progress, $total) {
  if ($total) {
    $part = round(($progress / $total) * 100);
    $whole = 100 - $part;
  }
  return '<div id="uts-dashboard-progess">' . ($total ?
           '<div id="uts-dashboard-progess-part" style="width: ' . $part . 'px;"></div>' .
           '<div id="uts-dashboard-progess-whole" style="width: ' . $whole . 'px;"></div>' : '') .
         '</div>';
}

/**
 * Send invitations to people asking them to participate.
 */
function uts_invite(&$form_state) {
  $form = array();

  // Get list of studies for select.
  $studies = uts_studies_load();
  $list = array();
  $list[] = t('--');
  foreach ($studies as $study) {
    $list[$study->nid] = $study->title;
  }

  $form['study_nid'] = array(
    '#type' => 'select',
    '#title' => t('Study'),
    '#options' => $list,
  );
  $form['addresses'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail addresses'),
    '#description' => t('List of e-mail addresses on separate lines to send the invites to. If the person\'s
                         name is specified using the syntax <em>Name, email@example.com</em> then the parameter
                         !name will use the person\'s name otherwise the e-mail address will be used.'),
    '#rows' => 10
  );
  $form['send'] = array(
    '#type' => 'submit',
    '#value' => t('Send')
  );

  return $form;
}

/**
 * Validate the addresses.
 */
function uts_invite_validate($form, &$form_state) {
  $addresses = explode("\n", $form_state['values']['addresses']);

  $line = 1;
  foreach ($addresses as $address) {
    $parts = explode(',', $address);
    if (count($parts) > 2) {
      form_set_error('addresses', t('To many comma separated parts on line @line.',
                                    array('@line' => $line)));
    }
    $email = trim(count($parts) == 1 ? $parts[0] : $parts[1]);
    if (!valid_email_address($email)) {
      form_set_error('addresses', t('Invalid e-mail address %address on line @line.',
                                    array('%address' => $email, '@line' => $line)));
    }
    $line++;
  }
}

/**
 * Send the invites.
 */
function uts_invite_submit($form, &$form_state) {
  global $user;
  $addresses = explode("\n", $form_state['values']['addresses']);

  // Cycle through addresses.
  foreach ($addresses as $address) {
    $params = array();
    $params['account'] = $user;

    $parts = explode(',', $address);
    if (count($parts) == 1) {
      $params['name'] = $to = trim($parts[0]);
    }
    else {
      $params['name'] = trim($parts[0]);
      $to = t('"@name" <!address>', array('@name' => $params['name'], '!address' => trim($parts[1])));
    }

    if ($form_state['values']['study_nid']) {
      $params['study_nid'] = $form_state['values']['study_nid'];
    }
    drupal_mail('uts', 'invite', $to, user_preferred_language($user), $params);
  }
  drupal_set_message(t('@count invites sent.', array('@count' => count($addresses))));
}

/**
 * Task import page.
 *
 * @param interger $study_nid Study NID to import tasks for.
 * @return array Form output.
 */
function uts_import_tasks(&$form_state, $study_nid = NULL) {
  $form = array();
  $form['study_nid'] = array(
    '#type' => 'value',
    '#value' => $study_nid
  );

  if ($study_nid) {
    uts_set_title($study_nid, 'Import tasks');

    // Only import sources from existing database if they will be related to a study.
    $form['existing_source'] = array(
      '#type' => 'fieldset',
      '#title' => t('Existing Source'),
      '#description' => t('Import tasks from database.'),
      '#weight' => -10
    );
    $form['existing_source']['tasks'] = array(
      '#type' => 'select',
      '#title' => t('Tasks'),
      '#multiple' => TRUE,
      '#weight' => -10
    );
    // Add options.
    $tasks = uts_tasks_load(NULL, TRUE);
    $options = array();
    foreach ($tasks as $task) {
      $options[$task->nid] = $task->title;
    }
    $form['existing_source']['tasks']['#options'] = $options;
    $form['existing_source']['tasks']['#size'] = min(6, count($options));
  }
  else {
    drupal_set_title(t('Import tasks'));
  }

  $form['xml_source'] = array(
    '#type' => 'fieldset',
    '#title' => t('XML Source'),
    '#description' => t('Import tasks from an XML source.'),
    '#weight' => -9
  );
  $form['xml_source']['file'] = array(
    '#type' => 'file',
    '#title' => t('Source'),
    '#description' => t('Correctly formed XML document containing UTS tasks.'),
    '#disabled' => TRUE, // TODO
    '#weight' => -10
  );

  $form['import'] = array(
    '#type' => 'submit',
    '#value' => 'Import',
    '#weight' => -8
  );

  return $form;
}

/**
 * Execute import either from XML or non-related tasks in database.
 */
function uts_import_tasks_submit($form, &$form_state) {
  // Tasks from database.
  if (isset($form_state['values']['tasks']) && count($form_state['values']['tasks']) > 0) {
    uts_tasks_import($form_state['values']['study_nid'], $form_state['values']['tasks']);
  }

  // TODO Tasks from XML.
}

/**
 * Menu callback -- ask for confirmation of task detaching then redirect
 * to appropriate overview page.
 */
function uts_task_detach_confirm(&$form_state, $task) {
  $type = node_get_types('type', $task);
  if ($type->orig_type != UTS_TASK) {
    drupal_set_message(t('Must be task to detach from study.'));
    drupal_goto('admin/uts/tasks');
  }
  $study = node_load($task->study_nid);
  $redirect = 'admin/uts/tasks/' . $study->nid;

  // Inject form values.
  $form['task_nid'] = array(
    '#type' => 'value',
    '#value' => $task->nid,
  );
  $form['redirect'] = array(
    '#type' => 'value',
    '#value' => $redirect,
  );

  return confirm_form($form,
    t('Are you sure you want to detach %task from %study?', array('%task' => $task->title, '%study' => $study->title)),
    isset($_GET['destination']) ? $_GET['destination'] : $redirect,
    t('This action cannot be undone.'),
    t('Detach'),
    t('Cancel')
  );
}

/**
 * Execute task detaching.
 */
function uts_task_detach_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $task = node_load($form_state['values']['task_nid']);
    $study = node_load($task->study_nid);
    db_query('UPDATE {uts_task}
                SET study_nid = NULL,
                weight = NULL
                WHERE nid = %d', $task->nid);
    drupal_set_message(t('%task was detached from %study.', array('%task' => $task->title, '%study' => $study->title)));
  }

  // Set the redirect to the pre-determined location.
  $form_state['redirect'] = $form_state['values']['redirect'];
}

/**
 * Menu callback -- ask for confirmation of node deletion then redirect
 * to appropriate overview page.
 */
function uts_delete_confirm(&$form_state, $node) {
  $message = t('This action cannot be undone.');

  // Decide on the redirect url.
  $type = node_get_types('type', $node);
  if ($type->orig_type == UTS_STUDY) {
    $redirect = 'admin/uts/studies';
    $message .= t(' Related tasks and session data will also be removed.');
  }
  else if ($type->orig_type == UTS_TASK) {
    $redirect = 'admin/uts/tasks' . ($node->study_nid ? '/' . $node->study_nid : '');
  }
  else if ($type->orig_type == UTS_ENVIRONMENT) {
    $redirect = 'admin/uts/environments';
  }

  // Inject form values.
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['redirect'] = array(
    '#type' => 'value',
    '#value' => $redirect,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $node->title)),
    isset($_GET['destination']) ? $_GET['destination'] : $redirect,
    $message,
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute node deletion.
 */
function uts_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    node_delete($form_state['values']['nid']);
  }

  // Set the redirect to the pre-determined location.
  $form_state['redirect'] = $form_state['values']['redirect'];
}
