<?php
/**
 * @file
 * Provide Usability Testing Suite data functions and hook implementations.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Get a list of all the data collection plug-ins available.
 *
 * @return array List of data colleciton plug-ins.
 * @see hook_uts_data_collection()
 */
function uts_data_collection_list() {
  return module_invoke_all('uts_data_collection');
}

/**
 * Get a list of client requirements.
 *
 * @return array List of client requirements.
 * @see hook_uts_client_requirements()
 */
function uts_data_client_requirements() {
  return module_invoke_all('uts_client_requirements');
}

/**
 * Get data collected from all enabled data plug-ins. Specified just the first
 * parameter or all the parameters.
 *
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @param interger $start_timestamp Start timestamp.
 * @param integer $stop_timestamp Stop timestamp.
 * @return array Data from plug-ins.
 * @see hook_uts_data_get()
 */
function uts_data_get($study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  return module_invoke_all('uts_data_get', $study_nid, $session_id, $start_timestamp, $stop_timestamp);
}

/**
 * Delete data stored by data collection plug-ins.
 *
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @see hook_uts_data_delete()
 */
function uts_data_delete($study_nid, $session_id = NULL) {
  module_invoke_all('uts_data_delete', $study_nid, $session_id);
}

/**
 * Get the start and stop timestamps for a given session. The function can
 * be called in the following three manors.
 *
 * ($sessiond_id)
 * ($sessiond_id, $start_and_stop_task_id)
 * ($sessiond_id, $start_task_id, $stop_task_id)
 *
 * @param string $session_id Session ID.
 * @param integer $start_task_id Start task ID (or both if no stop task specified).
 * @param integer $stop_task_id Stop task ID.
 * @param boolean $return_now Return time() if there is no stop timestamp. Only
 *   set to FALSE when calculating time spent on each task.
 *
 * @return array First element is the start time and second is the stop time.
 */
function uts_data_get_timestamps($session_id, $start_task_id = NULL, $stop_task_id = NULL, $return_now = TRUE) {
  if ($stop_task_id) {
    // All variable already set.
  }
  elseif ($start_task_id) {
    $stop_task_id = $start_task_id;
  }
  else {
    // Get first and last tasks in the study.
    $session = uts_session_load($session_id);
    $tasks = uts_tasks_load($session->study_nid);
    $first = array_shift($tasks);
    $last = ($tasks ? array_pop($tasks) : $first);

    $start_task_id = $first->nid;
    $stop_task_id = $last->nid;
  }
  $start = db_result(db_query("SELECT start_time
                               FROM {uts_session_task}
                               WHERE session_id = '%s'
                               AND task_nid = %d", $session_id, $start_task_id));
  $stop = db_result(db_query("SELECT stop_time
                              FROM {uts_session_task}
                              WHERE session_id = '%s'
                              AND task_nid = %d", $session_id, $stop_task_id));
  // Usually just want date range to query data from plugins.
  if ($return_now && $stop === FALSE) {
    $stop = time();
  }
  return array($start, $stop);
}

/**
 * Get the total time taken to complete a study.
 *
 * @param string $session_id Session ID to get total time taken.
 * @return interger Total time taken to complete study.
 */
function uts_data_get_total_time($session_id) {
  $session = uts_session_load($session_id);
  $study_nid = $session->study_nid;
  $tasks = uts_tasks_load($study_nid);

  // Cycle through each task and add up the time spent.
  $total = 0;
  foreach ($tasks as $task) {
    list($start, $stop) = uts_data_get_timestamps($session_id, $task->nid);
    $total += ($stop - $start);
  }
  return $total;
}

/**
 * Get a list of data analysis plug-ins.
 *
 * @return array List of data analysis plug-ins.
 * @see hook_uts_data_analysis()
 */
function uts_data_analysis_list() {
  return module_invoke_all('uts_data_analysis');
}

/**
 * Get a generated analysis from plug-ins.
 *
 * @param string $module Data collection module from which data will be retrieved.
 * @param string $mode The analysis mode.
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @param interger $start_timestamp Start timestamp.
 * @param integer $stop_timestamp Stop timestamp.
 * @return array Generated analysis.
 * @see hook_uts_data_display()
 */
function uts_data_analysis_display($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  return module_invoke_all('uts_data_display', $module, $mode, $study_nid, $session_id, $start_timestamp, $stop_timestamp);
}

/**
 * Get a generated analysis exportation from plug-ins.
 *
 * @param string $module Data collection module from which data will be retrieved.
 * @param string $mode The analysis mode.
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @param interger $start_timestamp Start timestamp.
 * @param integer $stop_timestamp Stop timestamp.
 * @return array Generated analysis.
 * @see hook_uts_data_analysis_export().
 */
function uts_data_analysis_export($module, $mode, $study_nid, $session_id = NULL, $start_timestamp = NULL, $stop_timestamp = NULL) {
  return module_invoke_all('uts_data_analysis_export', $module, $mode, $study_nid, $session_id, $start_timestamp, $stop_timestamp);
}
