<?php
/**
 * @file
 * Provide Usability Testing Suite testing environment functions.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Invoke the callback function in another process using a http request. This
 * ensures that static variables that are messed up during environment create
 * and destroy do no effect the user experience.
 *
 * @param string $op Operation to perform (create, destroy).
 * @param string $prefix Environment prefix to perform opperation on.
 * @param string $second If performing create opperation an environment
 *   be used as a base to copy from. If performing enabling or disabling
 *   modules then a comman separate list of modules can be specified.
 * @return boolean Success.
 */
function uts_environment_invoke_callback($op, $prefix, $second = NULL) {
  // Set internal request data for checking in other thread.
  variable_set('uts_environment', "$op:$prefix:$second");

  $header = array('User-Agent' => 'UTS-Environment-Callback');
  $url = url("uts/environment/$op/$prefix/$second", array('absolute' => TRUE));
  $result = drupal_http_request($url, $header);

  // Fix any issues after creating environment.
  uts_environment_rebuild_caches();

  if ($result->data != 'success') {
    return FALSE;
  }

  return TRUE;
}

/**
 * Menu callback that allows environment opperation to be called
 * in a seperate process via a http request.
 *
 * @param string $op Operation to perform (create, destroy).
 * @param string $prefix Environment prefix to perform opperation on.
 * @param string $second If performing create opperation an environment
 *   be used as a base to copy from. If performing enabling or disabling
 *   modules then a comman separate list of modules can be specified.
 * @return Output status code.
 */
function uts_environment_callback($op, $prefix, $second = NULL) {
  if (variable_get('uts_environment', '') != "$op:$prefix:$second" ||
      $_SERVER['HTTP_USER_AGENT'] != 'UTS-Environment-Callback') {
    // Invalid request.
    exit('error');
  }
  variable_del('uts_environment');

  if ($op == 'create') {
    uts_environment_create($prefix, $second);
  }
  else if ($op == 'destroy') {
    uts_environment_destroy($prefix);
  }
  else if ($op == 'enable_modules') {
    uts_environment_modules($prefix, $second, TRUE);
  }
  else if ($op == 'disable_modules') {
    uts_environment_modules($prefix, $second, FALSE);
  }
  else {
    exit('error');
  }
  exit('success');
}

/**
 * Create prefixed environment, including database and files directory.
 *
 * @param string $prefix Prefix to use for new environment.
 * @param string $copy_prefix Prefix of environment to copy.
 */
function uts_environment_create($prefix, $copy_prefix = NULL) {
  global $db_prefix;

  // Retreive variables before database is changed.
  $admin_name = variable_get('uts_environment_admin_name', 'admin');
  $admin_pass = variable_get('uts_environment_admin_pass', 'utsadmin');

  // Get max nid, and vid for use below.
  $result = db_fetch_object(db_query('SELECT MAX(nid) AS max_nid, MAX(vid) AS max_vid FROM {node}'));
  $max_nid = $result->max_nid;
  $max_vid = $result->max_vid;

  // Store necessary current values before switching to prefixed database.
  $db_prefix_original = $db_prefix;
  $clean_url_original = variable_get('clean_url', 0);

  // Generate temporary prefixed database to ensure that test sessions have a clean starting point.
  $db_prefix = $prefix;
  include_once './includes/install.inc';
  drupal_install_system();

  $modules = drupal_verify_profile('default', 'en');

  // Remove dblog to avoid extraneous errors while logging.
  foreach ($modules as $key => $module) {
    if ($module == 'dblog') {
      unset($modules[$key]);
    }
  }

  if ($copy_prefix && $copy_prefix != 'base') {
    // Set prefix to base environment database.
    $db_prefix = $copy_prefix;

    // Load modules from study base environment, and override default list.
    $modules = array();
    $result = db_query("SELECT name
                        FROM {system}
                        WHERE status = %d
                        AND type = '%s'", 1, 'module');
    while ($module = db_result($result)) {
      $modules[] = $module;
    }

    // Return to installation database.
    $db_prefix = $prefix;
  }
  else if ($copy_prefix == 'base') {
    // Add proctor to base installations only. The menu hooks for proctor
    // will not work for some reason.
    $modules[] = 'uts_proctor';
  }

  drupal_install_modules($modules);

  // Run default profile tasks.
  $task = 'profile';
  default_profile_tasks($task, '');

  // Rebuild caches.
  uts_environment_rebuild_caches();

  // Restore necessary variables.
  variable_set('install_profile', 'default');
  variable_set('install_task', 'profile-finished');
  variable_set('clean_url', $clean_url_original);

  // Use temporary files directory with the same prefix as database.
  variable_set('file_directory_path', file_directory_path() . '/' . $prefix);
  file_check_directory(file_directory_path(), TRUE); // Create the files directory.

  if ($copy_prefix && $copy_prefix != 'base') {
    // Override database.
    uts_environment_copy_database($copy_prefix, $prefix);

    // Copy files.
    uts_environment_copy_directory(file_directory_path() . "/../$copy_prefix", file_directory_path());
    file_put_contents('output.txt', file_directory_path() . "/../$copy_prefix\n", FILE_APPEND);
    file_put_contents('output.txt', file_directory_path() . "\n", FILE_APPEND);
  }
  else {
    // Create admin user if base or no prefixed environment to copy from.
    $pass_hash = md5($admin_pass);
    db_query("UPDATE {users}
              SET name = '%s',
                  pass = '%s',
                  created = %d,
                  access = %d,
                  login = %d,
                  status = %d
              WHERE uid = %d", $admin_name, $pass_hash, time(), 0, 0, 1, 1);
  }

  // Rebuild caches.
  uts_environment_rebuild_caches();

  // Add required UTS variables.
  variable_set('uts_db_prefix', $db_prefix_original);

  // Restore original environment.
  $db_prefix = $db_prefix_original;
  uts_environment_rebuild_caches();
}

/**
 * Copy one database to another, using the prefixes as different databases.
 *
 * @param string $original Original database prefix.
 * @param string $new New database prefix.
 */
function uts_environment_copy_database($original, $new) {
  file_put_contents('output.txt', '$copy_prefix => ' . $original . "\n");
  $tables = uts_environment_get_like_tables(''); // Already in database prefix
  foreach ($tables as $table) {
    // Empty table and then copy data.
    // Table variable has the prefix already in it. Format: $prefix$table.
    db_query('TRUNCATE TABLE ' . $table . '');
    if (!preg_match('/cache.*/', $table)) {
      // Let cache tables be re-generated.
      $table_original = $original . str_replace($new, '', $table);
      if (!preg_match('/uts\d+user/', $table)) {
        db_query('INSERT INTO ' . $table . '
                    SELECT *
                    FROM ' . $table_original);
      }
      else {
        // Make special exception for user table. Don't import user ID 0 due to issues.
        db_query('INSERT INTO ' . $table . '
                    SELECT *
                    FROM ' . $table_original . '
                    WHERE ' . $table_original . '.uid != 0');
      }
    }
    file_put_contents('output.txt', "" . $table_original . "\n", FILE_APPEND);
  }

  // Make proctor as disabled so that it when enabled it will install menu items.
  db_query("UPDATE {system}
            SET status = %d
            WHERE name = '%s'", 0, 'uts_proctor');
}

/**
 * Destroy prefixed environment.
 *
 * @param string $prefix Prefix of environment to destroy.
 */
function uts_environment_destroy($prefix) {
  global $db_prefix;

  // Delete temporary files directory and reset files directory path.
  uts_environment_destroy_directory(file_directory_path() . "/$prefix");

  // Remove all prefixed tables (all the tables in the schema).
  $tables = uts_environment_get_like_tables($prefix);
  $ret = array();
  foreach ($tables as $table) {
    if (!preg_match('/^uts_.*$/', $table)) {
      // UTS tables won't exist since they never have their hook_install() called.
      db_drop_table($ret, $table);
    }
  }
}

/**
 * Perform operations on modules, either enable or disable. Executes
 * system_modules form to ensure that all caches are cleared properly.
 *
 * @param string $prefix Prefix of environment to enable modules in.
 * @param string $modules Comma separated list of modules.
 * @param boolean $enable Enable/Disable list of modules.
 */
function uts_environment_modules($prefix, $modules, $enable) {
  global $db_prefix;

  // Switch database.
  $db_prefix_original = $db_prefix;
  $db_prefix = $prefix;
  uts_environment_rebuild_caches();

  // Prepare form_state array.
  $modules = explode(',', $modules);
  $form_state = array();
  foreach ($modules as $module) {
    $form_state['values']["status[$module]"] = $enable;
  }

  $reserved_modules = module_list(TRUE, FALSE);
  foreach ($reserved_modules as $module) {
    $form_state['values']["status[$module]"] = TRUE;
  }

  // Execute system_modules form to ensure that all caches are cleared properly.
  require_once drupal_get_path('module', 'system') . '/system.admin.inc';
  drupal_execute('system_modules', $form_state);

  // Changes don't get saved so force them.
  if ($enable) {
    module_enable($modules);
  }
  else {
    module_disable($modules);
  }
  uts_environment_rebuild_caches();

  // Restore original environment.
  $db_prefix = $db_prefix_original;
  uts_environment_rebuild_caches();
}

/**
 * Refresh all caches to ensure that static variables reflect current environment.
 */
function uts_environment_rebuild_caches() {
  global $conf;
  cache_clear_all();
  $conf = variable_init();

  module_list(TRUE, FALSE);
  module_rebuild_cache();
  drupal_flush_all_caches();
  menu_rebuild();

  actions_synchronize();
}

/**
 * Remove all files from specified directory and then remove directory.
 *
 * @param string $path Directory path.
 */
function uts_environment_destroy_directory($path) {
  if (is_dir($path)) {
    $files = scandir($path);
    foreach ($files as $file) {
      if ($file != '.' && $file != '..') {
        $file_path = "$path/$file";
        if (is_dir($file_path)) {
          uts_environment_destroy_directory($file_path);
        }
        else {
          file_delete($file_path);
        }
      }
    }
    rmdir($path);
  }
}

/**
 * Copy a directory recursively to another location.
 *
 * @param string $source Source directory location.
 * @param string $destination Destination directory location.
 */
function uts_environment_copy_directory($source, $destination) {
  if (is_dir($source)) {
    if (!file_exists($destination)) {
      mkdir($destination);
    }
    $files = scandir($source);
    foreach ($files as $file) {
      if ($file != '.' && $file != '..') {
        $file_path = "$source/$file";
        $new_path = str_replace($source . '/', $destination . '/', $file_path);
        if (is_dir($file_path)) {
          uts_environment_copy_directory($file_path, $new_path);
        }
        else {
          copy($file_path, $new_path);
        }
      }
    }
  }
}

/**
 * Find all tables that are like the specified base table name.
 *
 * @param string $base_table Base table name.
 * @param boolean $count Return the table count instead of list of tables.
 * @return mixed Array of matching tables or count of tables.
 */
function uts_environment_get_like_tables($base_table = 'uts', $count = FALSE) {
  global $db_url, $db_prefix;
  $url = parse_url($db_url);
  $database = substr($url['path'], 1);
  $select = $count ? 'COUNT(table_name)' : 'table_name';
  $result = db_query("SELECT $select
                      FROM information_schema.tables
                      WHERE table_schema = '%s'
                      AND table_name LIKE '%s%'", $database, $db_prefix . $base_table);

  if ($count) {
    return db_result($result);
  }
  $tables = array();
  while ($table = db_result($result)) {
    $tables[] = $table;
  }
  return $tables;
}

/**
 * Create table to list all the environments.
 *
 * @return string HTML output.
 */
function uts_environments() {
  $environments = uts_environments_load();
  $header = array(t('Prefix'), t('Name'), t('Description'), t('Operations'));
  $rows = array();
  foreach ($environments as $environment) {
    $row = array();
    $row[] = $environment->prefix;
    $row[] = $environment->title;
    $row[] = $environment->body;
    $row[] = uts_render_operations(uts_environment_operations($environment));

    $rows[] = $row;
  }

  if (empty($rows)) {
    return t('No environments to display.');
  }
  return theme('table', $header, $rows);
}

/**
 * Get an array containing the environment operation links.
 *
 * @param $environment Environment object.
 * @return Array of links.
 */
function uts_environment_operations($environment) {
  $links = array();
  $links['environment_edit'] = array(
    'title' => t('edit'),
    'href' => "admin/uts/environments/$environment->nid/edit"
  );
  $links['environment_open'] = array(
    'title' => t('open'),
    'href' => "admin/uts/environments/$environment->nid/open"
  );
  $links['environment_delete'] = array(
    'title' => t('delete'),
    'href' => "admin/uts/environments/$environment->nid/delete"
  );

  return $links;
}

/**
 * Load all environments.
 *
 * @return array List of environment objects.
 */
function uts_environments_load() {
  $result = db_query('SELECT nid
                      FROM {uts_environment}');

  $environments = array();
  while ($environment = db_result($result)) {
    $environments[] = node_load($environment);
  }
  return $environments;
}

/**
 * Open the specified environment. Only to be used with base environments.
 *
 * @param object $environment Environment node object.
 */
function uts_environment_open($environment) {
  uts_session_restore($environment->prefix);
  drupal_goto();
}
