<?php
/**
 * @file
 * Provide data collection API functions while running in the test environment.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Load current UTS session and place the values in the global variable
 * $uts_session. Values provided are listed below.
 *
 * session_id        The string sessiond ID.
 * study             The study (object) the session is apart of.
 * complete          The session is complete.
 * current_task      The current task (object).
 * running           The sesssion is currently "running" a task.
 * start_time        The time the task was started.
 * time_remaining    The remaining time left to complete task.
 *
 * @param boolean Reload session values.
 * @return boolean Session loaded.
 */
function uts_proctor_session_load($reload = FALSE) {
  global $uts_session;
  if (empty($uts_session) || $reload) {
    $session = uts_proctor_call_uts('uts_session_load', $_COOKIE['uts_session']);

    // Process session values.
    if (!empty($session)) {
      $uts_session['session_id'] = $session->session_id;
      $uts_session['study'] = uts_proctor_node_load('study', $session->study_nid);
      $uts_session['complete'] = $session->complete;
      $uts_session['current_task'] = uts_proctor_node_load('task', $session->current_task);
      $uts_session['running'] = $session->start_time != NULL;
      $uts_session['start_time'] = $session->start_time;
      $uts_session['time_remaining'] = $uts_session['current_task']->max_time - (time() - $uts_session['start_time']);
      return TRUE;
    }
    return FALSE;
  }
  return TRUE;
}

/**
 * Get the current timestamp.
 *
 * @return interger Timestamp.
 */
function uts_proctor_current_timestamp() {
  return time(); // TODO Decide on timezone etc.
}

/**
 * Check to see if data collection plug-ins should record data.
 *
 * @return boolean Should record data.
 */
function uts_proctor_data_record() {
  global $uts_session;

  if (uts_session_active()) {
    uts_proctor_session_load();

    return $uts_session['running'];
  }
  return FALSE;
}
