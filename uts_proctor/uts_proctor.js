
$(document).ready(function() {
  $('div#uts-task-dialog').dialog();
  $('div#uts-task-dialog').dialog('close');
});

$('span#uts-task-dialog-open').click(function() {
  $('div#uts-task-dialog').dialog(($('div#uts-task-dialog').dialog('isOpen') ? 'close' : 'open'));
});

function uts_proctor_display_task() {
  $('div#uts-task-dialog').dialog('open');
}

function uts_proctor_request_action() {
  $('#uts-proctor-interface').effect('pulsate', { times: 1 }, 1000);
}
