<?php
?>
<div id="uts-proctor-interface" class="uts-overlay">
  <div id="uts-study" class="uts-overlay-item">
    <?php print $study; ?>
  </div>
  <div id="uts-task" class="uts-overlay-item">
    <?php print $task; ?>
  </div>
  <div id="uts-actions" class="uts-overlay-item">
    <?php print $actions; ?>
  </div>
</div>

<?php if ($task_title) { ?>
  <div id="uts-task-dialog" title="<?php print $task_title; ?>" class="redmond">
  <?php print $task_body; ?>
  </div>
<?php } ?>
