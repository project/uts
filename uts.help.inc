<?php
/**
 * @file
 * Provide help text for Usability Testing Suite.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implementation of hook_help().
 */
function uts_help($path, $arg) {
  switch ($path) {
    case 'admin/help#uts':
      return t('<p>
                  For step-by-step instructions on how to setup a usability study see
                  <a href="@uts_admin">usability testing suite admin</a> page. If you plan on collaborating in
                  creating a study definition make sure you give permission to all the user that will be involved.
                 </p>
                 <p>
                   All plug-ins that you want to use on a study must be enabled to show up in the list when creating/editting
                   a study. Plug-ins are displayed on the <a href="@modules">modules</a> page under the group heading
                   <i>Usability testing suite - plugins</i>.
                 </p>
                 <p>
                   Any bugs, questions, or concerns should be placed in the
                   <a href="http://drupal.org/project/issues/uts">Usability Testing Suite issue queue</a>.
                 </p>', array('@uts_admin' => url('admin/uts'),
                              '@modules' => url('admin/build/modules')));
    case 'admin/uts/analyze':
      return t('<p>
                  Studies that are marked as <em>active</em> or <em>closed</em> and have completed sessions will be displayed below.
                  After selecting a study to analyze you will be presented with the analysis display.
                </p>');
    case 'admin/uts/environments':
      return t('<p>
                  Environments are used as the basis for the environment a participant will start with when beginning a study.
                  You can setup the environment how it bests fits the study. Once you have created an environment you can
                  <i>open</i> the environment and login as the super user using the username and password specified on the
                  <a href="@configuration">configuration</a> page
                </p>',
                array('@configuration' => url('admin/uts/configuration')));
    case 'admin/uts/invite':
      return t('<p>
                  The message to be sent can be modified on the <a href="@configuration">configuration</a> page.
                </p>',
                array('@configuration' => url('admin/uts/configuration')));
  }
}
