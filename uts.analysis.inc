<?php
/**
 * @file
 * Provide data analysis pages for the Usability Testing Suite.
 *
 * Copyright 2008 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Menu callback: List the studies that can be analyzed.
 *
 * @return string HTML Output.
 */
function uts_analysis_list() {
  $studies = array_merge(uts_studies_load(UTS_STUDY_STATUS_ACTIVE), uts_studies_load(UTS_STUDY_STATUS_CLOSED));
  $list = array();
  foreach ($studies as $study) {
    if (count(uts_session_load_all($study->nid, TRUE)) > 0) {
      $list[] = t('!title<br />@description',
                   array('!title' => l($study->title, 'admin/uts/analyze/' . $study->nid), '@description' => t($study->body)));
    }
  }

  if (empty($list)) {
    return t('No studies to analyze.');
  }
  return t('<p>Select a study to analyze.</p>!studies', array('!studies' => theme('item_list', $list)));
}

/**
 * Menu callback: Display table of sessions related to study.
 *
 * @param interger $study_nid Current study NID.
 * @return string HTML Output.
 */
function uts_analysis_list_sessions($study_nid) {
  uts_analysis_set_title($study_nid);

  drupal_add_js(drupal_get_path('module', 'uts') . '/uts.js');
  drupal_add_css(drupal_get_path('module', 'uts') . '/uts.css', 'module');
  jquery_ui_add('ui.accordion');

  $js = array(
    'images' => array(
       theme('image', 'misc/menu-collapsed.png', 'Expand', 'Expand'),
       theme('image', 'misc/menu-expanded.png', 'Collapsed', 'Collapsed'),
    ),
  );
  drupal_add_js(array('uts' => $js), 'setting');

  module_load_include('inc', 'uts', 'uts.pages');

  $study = node_load($study_nid);
  $progress = uts_dashboard_studies_progress($study);

  $tasks = uts_tasks_load($study_nid);
  $header = array(t('Name'), t('Participants'), t('Completion rate'));
  $rows = array();
  foreach ($tasks as $task) {
    $row = array();
    $row[] = '<div id="uts-task-open-' . $task->nid . '" class="uts-task-open"><span></span>' . $task->title . '</div>';
    $row[] = $progress[$task->nid]['participated'];
    $row[] = theme('uts_progress', $progress[$task->nid]['completed'], $progress[$task->nid]['participated']);

    $rows[] = array(
      'data' => $row,
      'class' => 'uts-task-head'
    );

    $row = array();
    $row[] = array(
      'data' => '<div id="uts-task-' . $task->nid . '" style="display: none;">' . uts_analysis_list_sessions_details($task) . '</div>',
      'colspan' => 3
    );

    $rows[] = array(
      'data' => $row,
      'class' => 'uts-task-details',
      'style' => 'display: none;'
    );
  }
  return '<div class="dashboard-main">' . theme('table', $header, $rows) . uts_analysis_list_sessions_links($study_nid) . '</div>' .
         '<div class="dashboard-sidebar">' . uts_analysis_list_sessions_status($study, $tasks, $progress) . '</div>';
}

/**
 * Create a summary of participant data for a particular task.
 *
 * @param object $task Task object.
 * @return string HTML output.
 */
function uts_analysis_list_sessions_details($task) {
  $out = '';

  // Lime spent chart.
  $chart = array(
    '#chart_id' => 'time_spent_' . $task->nid,
    '#title' => t('Time spent (seconds)'),
    '#type' => CHART_TYPE_BAR_V_GROUPED,
    '#size' => chart_size(250, 200),
    '#grid_lines' => chart_grid_lines(15, 20),
    '#bar_size' => chart_bar_size(20, 5),
    '#data' => array(),
    '#data_colors' => array()
  );

  $sessions = uts_session_load_all($task->study_nid);
  $max = 0;
  foreach ($sessions as $session_id) {
    $session = uts_session_load($session_id);
    list($start, $stop) = uts_data_get_timestamps($session->session_id, $task->nid, NULL, FALSE);
    $time = $stop - $start;
    $max = max($max, $time);

    $chart['#data'][] = array($time);
    $chart['#data_colors'][] = chart_unique_color($session->session_id, 'uts');
  }

  $chart['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][][] = chart_mixed_axis_range_label(0, $max);

  $out .= chart_render($chart);
  $out .= '<br />';

  // Live comments.
  $out .= '<b>' . t('Live feedback') . '</b>';

  $out .= '<div id="uts-live-feedback">';
  $out .= '<div id="uts-live-feedback-participants">';
  $out .= '<div id="uts-live-feedback-label">' . t('Participants') . ':</div> ';

  $data = array();
  $i = 1;
  foreach ($sessions as $session_id) {
    $out .= '<div class="uts-live-feedback-participant" style="background-color: #' . chart_unique_color($session_id, 'uts') .
            '">' . $i . '</div>';

    list($start, $stop) = uts_data_get_timestamps($session_id, $task->nid);
    if ($start) {
      $data[$session_id] = uts_data_get($task->study_nid, $session_id, $start, $stop);
      $data[$session_id] = $data[$session_id]['uts_live_feedback'];
    }
    else {
      $data[$session_id] = array(); // Add blank entry so that placeholder text is added.
    }
    $i++;
  }
  $out .= '</div>';

  $i = 1;
  foreach ($data as $session_id => $comments) {
    $out .= '<div id="uts-comments-' . $i . '" class="uts-live-feedback-comments">';
    $list = array();
    foreach ($comments as $comment) {
      $teaser = node_teaser($comment['message'], NULL, 50);
      $teaser = (drupal_strlen($comment['message']) > drupal_strlen($teaser) ? $teaser . '...' : $teaser);
      $list[] = l($teaser, '#') . '<div>' . $comment['message'] . '</div>';
    }

    $out .= ($list ? theme('item_list', $list, NULL, 'ul', array('class' => 'ui-accordion-container')) :
             '<em>' . t('No comments to display.') . '</em>');
    $out .= '</div>';
    $i++;
  }

  $out .= '</div>';

  return $out;
}

/**
 * Create analyze links for each participant.
 *
 * @param interger $study_nid Current study NID.
 * @return string HTML output.
 */
function uts_analysis_list_sessions_links($study_nid) {
  $sessions = uts_session_load_all($study_nid);

  $out = '<div id="uts-live-feedback-label">' . t('Analyze participant') . ':</div> ';

  $data = array();
  $i = 1;
  foreach ($sessions as $session_id) {
    $out .= '<a href="/admin/uts/analyze/' . $study_nid . '/' . $session_id . '">' .
            '<div class="uts-live-feedback-participant" style="background-color: #' . chart_unique_color($session_id, 'uts') . '">' . $i .
            '</div></a>';
    $i++;
  }
  return $out;
}

/**
 * Generate a table of study status information.
 *
 * @param object $study Study object.
 * @param array $tasks List of tasks related to study.
 * @param array $progress Progress information.
 * @return string HTML output.
 */
function uts_analysis_list_sessions_status($study, $tasks, $progress) {
  $out = '';
  $header = array(t('Key'), t('Value'));
  $rows = array();

  $rows[] = array(t('Status'), uts_get_study_status($study->study_status));
  $rows[] = array(t('Participants'), t('@count / @required',
              array('@count' => $progress['overal']['participated'], '@required' => $study->participant_count)));
  $rate = round(($progress['overal']['completed'] / (count($tasks) * $study->participant_count)) * 100);
  $rows[] = array(t('Completion rate'), $rate . '%');

  $out .= theme('table', $header, $rows);

  return $out;
}

/**
 * Menu callback: Display details about session.
 *
 * @param interger $study_nid Current study NID.
 * @param string $session_id Current session ID.
 * @return string HTML Output.
 */
function uts_analysis_session_details($study_nid, $sessiond_id) {
  uts_analysis_set_title($study_nid, $sessiond_id);

  return 'TODO: session details and destroy link.';
}

/**
 * Menu callback: Returns to sessions overview page.
 *
 * @param interger $study_nid Current study NID.
 * @return string HTML Output.
 */
function uts_analysis_sessions_goto($study_nid) {
  drupal_goto('admin/uts/analyze/' . $study_nid);
}

/**
 * Menu callback: Display particular plugin generated analysis.
 *
 * @param interger $study_nid Current study NID.
 * @param string $plugin Current analysis plug-in.
 * @param string $mode Current analysis mode.
 * @param string $session_id Current session ID.
 * @return string HTML Output.
 */
function uts_analysis($study_nid, $plugin, $mode = NULL, $session_id = NULL) {
  uts_analysis_set_title($study_nid, $session_id);

  if (!uts_analysis_check_parameters($study_nid, $plugin, $mode, $session_id)) {
    return '';
  }

  // Parameters have been checked, display data or perform action.
  // Get timestamp range.
  if ($session_id) {
    list($start, $stop) = uts_data_get_timestamps($session_id); // TODO Provide timestamp filter.
  }
  else {
    list($start, $stop) = array(NULL, NULL);
  }

  // Display data analysis from plug-in.
  $output = uts_analysis_display($plugin, $mode, $study_nid, $session_id, $start, $stop);

  // Add action links.
  $base = "admin/uts/analyze/$study_nid/$plugin/$mode";
  if ($session_id) {
    $base = "admin/uts/analyze/$study_nid/$session_id/$plugin/$mode";
  }
  foreach (array('export') as $action) {
    $output .= l(t('Export'), $base . "/$action");
  }

  return $output;
}

/**
 * Menu callback: Perform an action on analysis item.
 *
 * @param unknown_type $session_id
 * @param interger $study_nid Current study NID.
 * @param string $plugin Current analysis plug-in.
 * @param string $mode Current analysis mode.
 * @param string $action Action to perform.
 * @param string $session_id Current session ID.
 */
function uts_analysis_action($study_nid, $plugin, $mode, $action, $session_id = NULL) {
  if (!uts_analysis_check_parameters($study_nid, $plugin, $mode, $session_id)) {
    return '';
  }
  if (!in_array($action, array('export'))) {
    drupal_set_message(t('Invalid action.'), 'error');
    return '';
  }

  // Perform action.
  if ($action == 'export') {
    uts_analysis_export($plugin, $mode, $study_nid, $session_id, $start, $stop);
    exit();
  }
}

/**
 * Check the parameters to make sure they are valid.
 *
 * @param interger $study_nid Current study NID.
 * @param string $plugin Current analysis plug-in.
 * @param string $mode Current analysis mode.
 * @param string $session_id Current session ID.
 * @return boolean Valid parameters.
 */
function uts_analysis_check_parameters(&$study_nid, &$plugin, &$mode, &$session_id) {
  $studies = array_merge(uts_studies_load(UTS_STUDY_STATUS_ACTIVE), uts_studies_load(UTS_STUDY_STATUS_CLOSED));;
  $sessions = uts_session_load_all($study_nid);
  $analysis_plugins = uts_data_analysis_list();

  $found = FALSE;
  foreach ($studies as $study) {
    if ($study->nid == $study_nid) {
      $found = TRUE;
      break;
    }
  }
  if (!$found) {
    drupal_set_message(t('Invalid study NID.'), 'error');
    return FALSE;
  }

  if ($session_id && !in_array($session_id, $sessions)) {
    drupal_set_message(t('Invalid session ID.'), 'error');
    return FALSE;
  }

  if (!array_key_exists($plugin, $analysis_plugins)) {
    drupal_set_message(t('Invalid plugin.'), 'error');
    return FALSE;
  }

  if ($plugin && !$mode) {
    // Get default mode.
    $plugin_modes = $analysis_plugins[$plugin];
    $mode = $plugin_modes[0];
  }
  else if ($plugin && $mode) {
    $plugin_modes = $analysis_plugins[$plugin];
    if (!in_array($mode, $plugin_modes)) {
      drupal_set_message(t('Invalid plugin mode.'), 'error');
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Set title to include study and session ID if set.
 *
 * @param interger $study_nid Current study NID.
 * @param string $session_id Current session ID.
 */
function uts_analysis_set_title($study_nid, $session_id = NULL) {
  $study = node_load($study_nid);
  if ($session_id) {
    drupal_set_title(t('Analyze %study: %session', array('%study' => $study->title, '%session' => $session_id)));
  }
  else {
    drupal_set_title(t('Analyze %study', array('%study' => $study->title)));
  }
}

/**
 * Get the display elements from the analysis plug-ins, sort them by weight,
 * and collect the output.
 *
 * @param string $module Data colleciton module from which data will be retreived.
 * @param string $mode The analysis mode.
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @param interger $start_timestamp Start timestamp.
 * @param integer $stop_timestamp Stop timestamp.
 * @return string HTML output.
 */
function uts_analysis_display($plugin, $mode, $study_nid, $session_id, $start_timestamp, $stop_timestamp) {
  $elements = uts_data_analysis_display($plugin, $mode, $study_nid, $session_id, $start_timestamp, $stop_timestamp);
  uasort($elements, 'element_sort');

  // Output all the elements which have been sorted by weight.
  $output = '';
  foreach ($elements as $element) {
    $output .= $element['#data'];
  }
  return $output;
}

/**
 * Collection export files from analysis plug-ins, create ZIP archive, and
 * output to browser.
 *
 * @param string $module Data colleciton module from which data will be retreived.
 * @param string $mode The analysis mode.
 * @param interger $study_nid Study NID.
 * @param string $session_id Session ID.
 * @param interger $start_timestamp Start timestamp.
 * @param integer $stop_timestamp Stop timestamp.
 */
function uts_analysis_export($plugin, $mode, $study_nid, $session_id, $start_timestamp, $stop_timestamp) {
  require_once drupal_get_path('module', 'uts') . '/uts.zip.inc';

  // Get data and generate ZIP file.
  $exports = uts_data_analysis_export($plugin, $mode, $study_nid, $session_id, $start_timestamp, $stop_timestamp);
  $zip = new ZipFile();
  foreach ($exports as $file => $data) {
    $zip->addFile($data, $file);
  }

  // Output file.
  header("Content-Disposition: attachment; filename=\"$plugin-$mode.zip\"");
  header("Content-Type: application/zip");
  header("Content-Transfer-Encoding: binary");

  echo $zip->file();
}

/**
 * Get an array containing the session operation links.
 *
 * @param $session Session object.
 * @return Array of links.
 */
function uts_session_operations($session) {
  $links = array();
  if ($session->complete) {
    $links['analyze'] = array(
      'title' => t('analyze'),
      'href' => "admin/uts/analyze/$session->study_nid/$session->session_id"
    );
  }
  $links['session_destroy'] = array(
    'title' => t('destroy'),
    'href' => "admin/uts/analyze/$session->study_nid/$session->session_id/destroy"
  );
  return $links;
}

/**
 * Menu callback -- ask for confirmation of session destruction then redirect
 * to appropriate overview page.
 */
function uts_analysis_destroy_confirm(&$form_state, $session_id) {
  $redirect = 'admin/uts/analyze/' . arg(3);

  // Inject form values.
  $form['session_id'] = array(
    '#type' => 'value',
    '#value' => $session_id,
  );
  $form['redirect'] = array(
    '#type' => 'value',
    '#value' => $redirect,
  );

  return confirm_form($form,
    t('Are you sure you want to delete %session?', array('%session' => $session_id)),
    isset($_GET['destination']) ? $_GET['destination'] : $redirect,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute session destruction.
 */
function uts_analysis_destroy_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    uts_session_destory($form_state['values']['session_id']);
  }

  // Set the redirect to the pre-determined location.
  $form_state['redirect'] = $form_state['values']['redirect'];
}
